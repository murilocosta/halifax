package domain

import (
	"time"

	"bitbucket.org/murilocosta/halifax/core"
)

type Venda struct {
	Id         int64
	Cod        string
	TotalBruto float64
	Desconto   float64
	Emissao    time.Time
}

func NewVenda(cod string, totalBruto float64, desconto float64, emissao time.Time) *Venda {
	return &Venda{
		Cod:        cod,
		TotalBruto: totalBruto,
		Desconto:   desconto,
		Emissao:    emissao,
	}
}

func (c *Venda) Update(cod string) {
	c.Cod = cod
}

type ProdutoVenda struct {
	Id      int64
	VendaId int64
	ProdId  int64
	ValUnit float64
	Qtd     int32
}

func NewProdutoVenda(prodId int64, valUnit float64, qtd int32) *ProdutoVenda {
	return &ProdutoVenda{
		ProdId:  prodId,
		ValUnit: valUnit,
		Qtd:     qtd,
	}
}

func (p *ProdutoVenda) Update(vendaId int64) {
	p.VendaId = vendaId
}

type OperacaoVenda struct {
	Venda *Venda
	Itens []*ProdutoVenda
}

type VendaRepository interface {
	Save(c *OperacaoVenda) error
	Update(c *Venda) error
	FindById(id int64) (*Venda, error)
	FindOperacaoById(vendaId int64) (*OperacaoVenda, error)
	FindAll(p *core.Pagination) ([]*Venda, error)
}
