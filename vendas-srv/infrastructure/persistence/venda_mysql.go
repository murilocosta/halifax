package persistence

import (
	"database/sql"
	"strings"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/vendas-srv/domain"

	"github.com/micro/go-micro/util/log"
)

const (
	insertVendaQuery         = "INSERT INTO vendas (cod, total_bruto, desconto, emissao) VALUES (?, ?, ?, ?);"
	insertProdutoQuery       = "INSERT INTO venda_produtos (venda_id, prod_id, val_unit, qtd) VALUES "
	insertProdutoQueryParams = "(?, ?, ?, ?),"
	updateQuery              = "UPDATE vendas SET cod = ? WHERE id = ?;"
	findByIdQuery            = "SELECT id, cod, total_bruto, desconto, emissao FROM vendas WHERE id = ?;"
	findByOperacaoIdQuery    = "SELECT c.id, c.cod, c.total_bruto, c.desconto, c.emissao, p.id, p.venda_id, p.prod_id, p.val_unit, p.qtd FROM vendas AS c INNER JOIN venda_produtos AS p ON p.venda_id = c.id WHERE c.id = ?"
	findAllQuery             = "SELECT id, cod, total_bruto, desconto, emissao FROM vendas LIMIT ? OFFSET ?"
)

type vendaRepositoryMysql struct {
	db *sql.DB
}

func NewVendaRepositoryMysql(db *sql.DB) domain.VendaRepository {
	return &vendaRepositoryMysql{db: db}
}

func (r *vendaRepositoryMysql) Save(c *domain.OperacaoVenda) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	stmt, _ := tx.Prepare(insertVendaQuery)
	rs, err := stmt.Exec(c.Venda.Cod, c.Venda.TotalBruto, c.Venda.Desconto, c.Venda.Emissao)
	if err != nil {
		return tx.Rollback()
	}
	c.Venda.Id, _ = rs.LastInsertId()

	batchQuery := insertProdutoQuery
	var params []interface{}
	for _, v := range c.Itens {
		batchQuery += insertProdutoQueryParams
		params = append(params, c.Venda.Id, v.ProdId, v.ValUnit, v.Qtd)
	}
	batchQuery = strings.TrimSuffix(batchQuery, ",")

	stmt, err = tx.Prepare(batchQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(params)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *vendaRepositoryMysql) Update(c *domain.Venda) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	stmt, _ := tx.Prepare(updateQuery)
	_, err = stmt.Exec(c.Cod, c.Id)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *vendaRepositoryMysql) FindById(id int64) (*domain.Venda, error) {
	rows, err := r.db.Query(findByIdQuery, id)
	if err != nil {
		return nil, err
	}
	var cmp domain.Venda
	for rows.Next() {
		readVendaFromRows(rows, &cmp)
	}
	return &cmp, nil
}

func (r *vendaRepositoryMysql) FindOperacaoById(vendaId int64) (*domain.OperacaoVenda, error) {
	rows, err := r.db.Query(findByOperacaoIdQuery, vendaId)
	if err != nil {
		return nil, err
	}

	var cmp domain.Venda
	var prds []*domain.ProdutoVenda
	for rows.Next() {
		var prd domain.ProdutoVenda
		readOperacaoFromRows(rows, &cmp, &prd)
		prds = append(prds, &prd)
	}

	op := &domain.OperacaoVenda{
		Venda: &cmp,
		Itens: prds,
	}
	return op, nil
}

func (r *vendaRepositoryMysql) FindAll(p *core.Pagination) ([]*domain.Venda, error) {
	rows, err := r.db.Query(findAllQuery, p.GetSize(), p.GetPage())
	if err != nil {
		return nil, err
	}
	var cmps []*domain.Venda
	for rows.Next() {
		var cmp domain.Venda
		readVendaFromRows(rows, &cmp)
		cmps = append(cmps, &cmp)
	}
	return nil, nil
}

func readVendaFromRows(rows *sql.Rows, cmp *domain.Venda) error {
	return rows.Scan(
		cmp.Id,
		cmp.Cod,
		cmp.TotalBruto,
		cmp.Desconto,
		cmp.Emissao,
	)
}

func readOperacaoFromRows(rows *sql.Rows, cmp *domain.Venda, prd *domain.ProdutoVenda) error {
	return rows.Scan(
		cmp.Id,
		cmp.Cod,
		cmp.TotalBruto,
		cmp.Desconto,
		cmp.Emissao,
		prd.Id,
		prd.VendaId,
		prd.ProdId,
		prd.ValUnit,
		prd.Qtd,
	)
}
