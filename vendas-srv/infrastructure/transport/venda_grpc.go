package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/vendas-srv/application"
	"bitbucket.org/murilocosta/halifax/vendas-srv/domain"

	venda "bitbucket.org/murilocosta/halifax/vendas-srv/proto/vendas"
)

type vendaHandlerGrpc struct {
	ucase *application.VendaUseCase
}

func NewVendaHandlerGrpc(ucase *application.VendaUseCase) venda.VendaHandler {
	return &vendaHandlerGrpc{
		ucase: ucase,
	}
}

func (h *vendaHandlerGrpc) Criar(ctx context.Context, req *venda.CriarRequest, rsp *venda.CriarResponse) error {
	emissao := core.ConvertFromTimestamp(req.Venda.Emissao)
	comp := domain.NewVenda(req.Venda.Cod, req.Venda.TotalBruto, req.Venda.Desconto, emissao)

	var itens []*domain.ProdutoVenda
	for _, v := range req.Itens {
		item := domain.NewProdutoVenda(v.ProdId, v.ValUnit, v.Qtd)
		itens = append(itens, item)
	}

	cId, err := h.ucase.Criar(&domain.OperacaoVenda{
		Venda: comp,
		Itens: itens,
	})

	if err != nil {
		return err
	}
	rsp.Id = cId
	return nil
}

func (h *vendaHandlerGrpc) Alterar(ctx context.Context, req *venda.AlterarRequest, rsp *venda.AlterarResponse) error {
	cId, err := h.ucase.Alterar(req.Id, req.Cod)
	if err != nil {
		return err
	}
	rsp.Id = cId
	return nil
}

func (h *vendaHandlerGrpc) Buscar(ctx context.Context, req *venda.BuscarRequest, rsp *venda.BuscarResponse) error {
	o, err := h.ucase.Buscar(req.Id)
	if err != nil {
		return err
	}
	convertToVendaMessage(o.Venda, rsp.Venda)
	for _, v := range o.Itens {
		var prod venda.ProdutoVendaMessage
		convertToVendaProdutoMessage(v, &prod)
		rsp.Itens = append(rsp.Itens, &prod)
	}
	return nil
}

func (h *vendaHandlerGrpc) Listar(ctx context.Context, req *venda.ListarRequest, rsp venda.Venda_ListarStream) error {
	pg := core.NewPagination(req.Page, req.Size)
	vendas, err := h.ucase.Listar(pg)
	if err != nil {
		return err
	}
	for _, c := range vendas {
		var message venda.VendaMessage
		convertToVendaMessage(c, &message)
		rsp.Send(&message)
	}
	return rsp.Close()
}

func convertToVendaMessage(c *domain.Venda, message *venda.VendaMessage) {
	message.Id = c.Id
	message.Desconto = c.Desconto
	message.TotalBruto = c.TotalBruto
	message.Cod = c.Cod
	message.Emissao = core.ConvertToTimestamp(c.Emissao)
}

func convertToVendaProdutoMessage(p *domain.ProdutoVenda, message *venda.ProdutoVendaMessage) {
	message.Id = p.Id
	message.VendaId = p.VendaId
	message.ProdId = p.ProdId
	message.ValUnit = p.ValUnit
	message.Qtd = p.Qtd
}
