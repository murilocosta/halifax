package application

import (
	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/vendas-srv/domain"
)

type VendaUseCase struct {
	compraRepo domain.VendaRepository
}

func NewVendaUseCase(compraRepo domain.VendaRepository) *VendaUseCase {
	return &VendaUseCase{
		compraRepo: compraRepo,
	}
}

func (ucase *VendaUseCase) Criar(unit *domain.OperacaoVenda) (int64, error) {
	err := ucase.compraRepo.Save(unit)
	if err != nil {
		return -1, err
	}
	return unit.Venda.Id, nil
}

func (ucase *VendaUseCase) Alterar(id int64, cod string) (int64, error) {
	c, err := ucase.compraRepo.FindById(id)
	if err != nil {
		return -1, err
	}
	c.Update(cod)
	err = ucase.compraRepo.Update(c)
	if err != nil {
		return -1, err
	}
	return c.Id, nil
}

func (ucase *VendaUseCase) Buscar(id int64) (*domain.OperacaoVenda, error) {
	return ucase.compraRepo.FindOperacaoById(id)
}

func (ucase *VendaUseCase) Listar(pg *core.Pagination) ([]*domain.Venda, error) {
	return ucase.compraRepo.FindAll(pg)
}
