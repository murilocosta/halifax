package domain

import "bitbucket.org/murilocosta/halifax/core"

type Produto struct {
	Id        int64
	Cod       string
	Descricao string
}

func NewProduto(code string, descricao string) *Produto {
	return &Produto{
		Cod:       code,
		Descricao: descricao,
	}
}

func (p *Produto) Update(cod string, descricao string) {
	p.Cod = cod
	p.Descricao = descricao
}

type ProdutoRepository interface {
	Save(p *Produto) error
	Update(p *Produto) error
	FindById(id int64) (*Produto, error)
	FindAll(pg *core.Pagination) ([]*Produto, error)
}
