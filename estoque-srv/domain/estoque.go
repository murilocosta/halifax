package domain

import "bitbucket.org/murilocosta/halifax/core"

type Estoque struct {
	Id   int64
	Nome string
}

func NewEstoque(nome string) *Estoque {
	return &Estoque{
		Nome: nome,
	}
}

func (e *Estoque) Update(nome string) {
	e.Nome = nome
}

type EstoqueRepository interface {
	Save(e *Estoque) error
	Update(e *Estoque) error
	FindById(id int64) (*Estoque, error)
	FindAll(pg *core.Pagination) ([]*Estoque, error)
}
