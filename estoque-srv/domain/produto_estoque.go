package domain

type ProdutoEstoque struct {
	Id        int64
	ProdutoId int64
	EstoqueId int64
	Qtd       int32
}

func NewProdutoEstoque(estoqueId int64, produtoId int64, qtd int32) *ProdutoEstoque {
	return &ProdutoEstoque{
		ProdutoId: produtoId,
		EstoqueId: estoqueId,
		Qtd:       qtd,
	}
}

func (pe *ProdutoEstoque) Adicionar(qtd int32) {
	pe.Qtd += qtd
}

func (pe *ProdutoEstoque) Remover(qtd int32) {
	pe.Qtd -= qtd
}

type produtoQtd struct {
	Prod *Produto
	Qtd  int32
}

type OperacaoEstoque struct {
	Estoque  *Estoque
	Produtos []*produtoQtd
}

func (o *OperacaoEstoque) AdicionarProduto(p *Produto, qtd int32) {
	o.Produtos = append(o.Produtos, &produtoQtd{
		Prod: p,
		Qtd:  qtd,
	})
}

func (o *OperacaoEstoque) GetProdutos() []*produtoQtd {
	return o.Produtos
}

type DisponibilidadeEstoque struct {
	Disponivel    bool
	QtdDisponivel int32
}

type ProdutoEstoqueRepository interface {
	Save(p *ProdutoEstoque) error
	Update(p *ProdutoEstoque) error
	FindById(id int64) (*ProdutoEstoque, error)
	FindByEstoqueAndProdutoId(estoqueId int64, produtoId int64) (*ProdutoEstoque, error)
	FindAllProdutosByEstoqueId(estoqueId int64) (*OperacaoEstoque, error)
	IsAvailable(estoqueId int64, produtoId int64, qtd int32) (*DisponibilidadeEstoque, error)
}
