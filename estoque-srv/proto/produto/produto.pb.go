// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/produto/produto.proto

package go_micro_srv_estoque

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ProdutoMessage struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Cod                  string   `protobuf:"bytes,2,opt,name=cod,proto3" json:"cod,omitempty"`
	Descricao            string   `protobuf:"bytes,3,opt,name=descricao,proto3" json:"descricao,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ProdutoMessage) Reset()         { *m = ProdutoMessage{} }
func (m *ProdutoMessage) String() string { return proto.CompactTextString(m) }
func (*ProdutoMessage) ProtoMessage()    {}
func (*ProdutoMessage) Descriptor() ([]byte, []int) {
	return fileDescriptor_18839a41b5b8575e, []int{0}
}

func (m *ProdutoMessage) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProdutoMessage.Unmarshal(m, b)
}
func (m *ProdutoMessage) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProdutoMessage.Marshal(b, m, deterministic)
}
func (m *ProdutoMessage) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProdutoMessage.Merge(m, src)
}
func (m *ProdutoMessage) XXX_Size() int {
	return xxx_messageInfo_ProdutoMessage.Size(m)
}
func (m *ProdutoMessage) XXX_DiscardUnknown() {
	xxx_messageInfo_ProdutoMessage.DiscardUnknown(m)
}

var xxx_messageInfo_ProdutoMessage proto.InternalMessageInfo

func (m *ProdutoMessage) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ProdutoMessage) GetCod() string {
	if m != nil {
		return m.Cod
	}
	return ""
}

func (m *ProdutoMessage) GetDescricao() string {
	if m != nil {
		return m.Descricao
	}
	return ""
}

type CriarProdutoRequest struct {
	Produto              *ProdutoMessage `protobuf:"bytes,1,opt,name=Produto,proto3" json:"Produto,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *CriarProdutoRequest) Reset()         { *m = CriarProdutoRequest{} }
func (m *CriarProdutoRequest) String() string { return proto.CompactTextString(m) }
func (*CriarProdutoRequest) ProtoMessage()    {}
func (*CriarProdutoRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_18839a41b5b8575e, []int{1}
}

func (m *CriarProdutoRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CriarProdutoRequest.Unmarshal(m, b)
}
func (m *CriarProdutoRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CriarProdutoRequest.Marshal(b, m, deterministic)
}
func (m *CriarProdutoRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CriarProdutoRequest.Merge(m, src)
}
func (m *CriarProdutoRequest) XXX_Size() int {
	return xxx_messageInfo_CriarProdutoRequest.Size(m)
}
func (m *CriarProdutoRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CriarProdutoRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CriarProdutoRequest proto.InternalMessageInfo

func (m *CriarProdutoRequest) GetProduto() *ProdutoMessage {
	if m != nil {
		return m.Produto
	}
	return nil
}

type CriarProdutoResponse struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CriarProdutoResponse) Reset()         { *m = CriarProdutoResponse{} }
func (m *CriarProdutoResponse) String() string { return proto.CompactTextString(m) }
func (*CriarProdutoResponse) ProtoMessage()    {}
func (*CriarProdutoResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_18839a41b5b8575e, []int{2}
}

func (m *CriarProdutoResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CriarProdutoResponse.Unmarshal(m, b)
}
func (m *CriarProdutoResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CriarProdutoResponse.Marshal(b, m, deterministic)
}
func (m *CriarProdutoResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CriarProdutoResponse.Merge(m, src)
}
func (m *CriarProdutoResponse) XXX_Size() int {
	return xxx_messageInfo_CriarProdutoResponse.Size(m)
}
func (m *CriarProdutoResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CriarProdutoResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CriarProdutoResponse proto.InternalMessageInfo

func (m *CriarProdutoResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

type AlterarProdutoRequest struct {
	Produto              *ProdutoMessage `protobuf:"bytes,1,opt,name=Produto,proto3" json:"Produto,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *AlterarProdutoRequest) Reset()         { *m = AlterarProdutoRequest{} }
func (m *AlterarProdutoRequest) String() string { return proto.CompactTextString(m) }
func (*AlterarProdutoRequest) ProtoMessage()    {}
func (*AlterarProdutoRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_18839a41b5b8575e, []int{3}
}

func (m *AlterarProdutoRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AlterarProdutoRequest.Unmarshal(m, b)
}
func (m *AlterarProdutoRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AlterarProdutoRequest.Marshal(b, m, deterministic)
}
func (m *AlterarProdutoRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AlterarProdutoRequest.Merge(m, src)
}
func (m *AlterarProdutoRequest) XXX_Size() int {
	return xxx_messageInfo_AlterarProdutoRequest.Size(m)
}
func (m *AlterarProdutoRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_AlterarProdutoRequest.DiscardUnknown(m)
}

var xxx_messageInfo_AlterarProdutoRequest proto.InternalMessageInfo

func (m *AlterarProdutoRequest) GetProduto() *ProdutoMessage {
	if m != nil {
		return m.Produto
	}
	return nil
}

type AlterarProdutoResponse struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AlterarProdutoResponse) Reset()         { *m = AlterarProdutoResponse{} }
func (m *AlterarProdutoResponse) String() string { return proto.CompactTextString(m) }
func (*AlterarProdutoResponse) ProtoMessage()    {}
func (*AlterarProdutoResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_18839a41b5b8575e, []int{4}
}

func (m *AlterarProdutoResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AlterarProdutoResponse.Unmarshal(m, b)
}
func (m *AlterarProdutoResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AlterarProdutoResponse.Marshal(b, m, deterministic)
}
func (m *AlterarProdutoResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AlterarProdutoResponse.Merge(m, src)
}
func (m *AlterarProdutoResponse) XXX_Size() int {
	return xxx_messageInfo_AlterarProdutoResponse.Size(m)
}
func (m *AlterarProdutoResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_AlterarProdutoResponse.DiscardUnknown(m)
}

var xxx_messageInfo_AlterarProdutoResponse proto.InternalMessageInfo

func (m *AlterarProdutoResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

type ListarProdutosRequest struct {
	Page                 int32    `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Size                 int32    `protobuf:"varint,2,opt,name=size,proto3" json:"size,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListarProdutosRequest) Reset()         { *m = ListarProdutosRequest{} }
func (m *ListarProdutosRequest) String() string { return proto.CompactTextString(m) }
func (*ListarProdutosRequest) ProtoMessage()    {}
func (*ListarProdutosRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_18839a41b5b8575e, []int{5}
}

func (m *ListarProdutosRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListarProdutosRequest.Unmarshal(m, b)
}
func (m *ListarProdutosRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListarProdutosRequest.Marshal(b, m, deterministic)
}
func (m *ListarProdutosRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListarProdutosRequest.Merge(m, src)
}
func (m *ListarProdutosRequest) XXX_Size() int {
	return xxx_messageInfo_ListarProdutosRequest.Size(m)
}
func (m *ListarProdutosRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListarProdutosRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListarProdutosRequest proto.InternalMessageInfo

func (m *ListarProdutosRequest) GetPage() int32 {
	if m != nil {
		return m.Page
	}
	return 0
}

func (m *ListarProdutosRequest) GetSize() int32 {
	if m != nil {
		return m.Size
	}
	return 0
}

func init() {
	proto.RegisterType((*ProdutoMessage)(nil), "go.micro.srv.estoque.ProdutoMessage")
	proto.RegisterType((*CriarProdutoRequest)(nil), "go.micro.srv.estoque.CriarProdutoRequest")
	proto.RegisterType((*CriarProdutoResponse)(nil), "go.micro.srv.estoque.CriarProdutoResponse")
	proto.RegisterType((*AlterarProdutoRequest)(nil), "go.micro.srv.estoque.AlterarProdutoRequest")
	proto.RegisterType((*AlterarProdutoResponse)(nil), "go.micro.srv.estoque.AlterarProdutoResponse")
	proto.RegisterType((*ListarProdutosRequest)(nil), "go.micro.srv.estoque.ListarProdutosRequest")
}

func init() { proto.RegisterFile("proto/produto/produto.proto", fileDescriptor_18839a41b5b8575e) }

var fileDescriptor_18839a41b5b8575e = []byte{
	// 302 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x92, 0x5f, 0x4b, 0xc3, 0x30,
	0x14, 0xc5, 0xd7, 0xce, 0x6e, 0xec, 0x0a, 0x43, 0xe2, 0x26, 0xa5, 0xfa, 0x30, 0x82, 0x48, 0xfd,
	0x43, 0x94, 0xf9, 0xae, 0x88, 0xaf, 0x0a, 0x23, 0x20, 0x3e, 0x6a, 0x6d, 0x63, 0x09, 0xa8, 0xe9,
	0x72, 0x53, 0x1f, 0xfc, 0x5c, 0x7e, 0x40, 0x59, 0xda, 0x75, 0x4c, 0x33, 0xe8, 0x83, 0x4f, 0xbd,
	0x1c, 0x4e, 0x4e, 0x7e, 0xf7, 0x34, 0xb0, 0x5f, 0x68, 0x65, 0xd4, 0x79, 0xa1, 0x55, 0x56, 0xae,
	0xbe, 0xcc, 0xaa, 0x64, 0x94, 0x2b, 0xf6, 0x2e, 0x53, 0xad, 0x18, 0xea, 0x4f, 0x26, 0xd0, 0xa8,
	0x79, 0x29, 0xe8, 0x0c, 0x86, 0xb3, 0xca, 0x76, 0x2f, 0x10, 0x93, 0x5c, 0x90, 0x21, 0xf8, 0x32,
	0x0b, 0xbd, 0x89, 0x17, 0x77, 0xb9, 0x2f, 0x33, 0xb2, 0x03, 0xdd, 0x54, 0x65, 0xa1, 0x3f, 0xf1,
	0xe2, 0x01, 0x5f, 0x8c, 0xe4, 0x00, 0x06, 0x99, 0xc0, 0x54, 0xcb, 0x34, 0x51, 0x61, 0xd7, 0xea,
	0x2b, 0x81, 0x3e, 0xc0, 0xee, 0xad, 0x96, 0x89, 0xae, 0x63, 0xb9, 0x98, 0x97, 0x02, 0x0d, 0xb9,
	0x82, 0x7e, 0xad, 0xd8, 0xec, 0xed, 0xe9, 0x21, 0x73, 0x01, 0xb1, 0x75, 0x1a, 0xbe, 0x3c, 0x44,
	0x8f, 0x60, 0xb4, 0x1e, 0x8b, 0x85, 0xfa, 0xc0, 0x3f, 0xb8, 0xf4, 0x11, 0xc6, 0x37, 0x6f, 0x46,
	0xe8, 0x7f, 0x07, 0x88, 0x61, 0xef, 0x77, 0xf0, 0x06, 0x84, 0x6b, 0x18, 0xdf, 0x49, 0x34, 0x8d,
	0x11, 0x97, 0x08, 0x04, 0xb6, 0x8a, 0x24, 0x17, 0xd6, 0x1a, 0x70, 0x3b, 0x2f, 0x34, 0x94, 0x5f,
	0xc2, 0xf6, 0x1b, 0x70, 0x3b, 0x4f, 0xbf, 0xfd, 0x86, 0x95, 0x3c, 0x43, 0x60, 0xf7, 0x26, 0xc7,
	0x6e, 0x5c, 0x47, 0xd7, 0xd1, 0x49, 0x1b, 0x6b, 0x05, 0x4f, 0x3b, 0xe4, 0x15, 0xfa, 0xf5, 0x62,
	0xe4, 0xd4, 0x7d, 0xd0, 0x59, 0x68, 0x74, 0xd6, 0xce, 0xdc, 0xdc, 0xf3, 0x04, 0xbd, 0xaa, 0x96,
	0x4d, 0xd7, 0x38, 0x4b, 0x8b, 0x5a, 0xfd, 0x26, 0xda, 0xb9, 0xf0, 0x5e, 0x7a, 0xf6, 0xa1, 0x5f,
	0xfe, 0x04, 0x00, 0x00, 0xff, 0xff, 0xcf, 0x01, 0x27, 0x8a, 0x07, 0x03, 0x00, 0x00,
}
