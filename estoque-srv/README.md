# Estoque Service

This is the Estoque service

Generated with

```
micro new bitbucket.org/murilocosta/halifax/estoque-srv --namespace=go.micro --type=srv --plugin=registry=rabbit
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: go.micro.srv.estoque
- Type: srv
- Alias: estoque

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./estoque-srv
```

Build a docker image
```
make docker
```