package subscriber

import (
	"context"

	"github.com/micro/go-micro/util/log"

	_ "bitbucket.org/murilocosta/halifax/estoque-srv/proto/estoque"
)

type Estoque struct{}

func (e *Estoque) Handle(ctx context.Context, msg string) error {
	log.Log("Handler Received message: ", msg)
	return nil
}
