package persistence

import (
	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"
	"database/sql"

	"github.com/micro/go-micro/util/log"
)

const (
	produtoInsertQuery = "INSERT INTO produtos (cod, descricao) VALUES (?, ?);"
	produtoUpdateQuery = "UPDATE produtos SET cod = ?, descricao = ? WHERE id = ?;"
	produtoFindByIdQuery = "SELECT id, cod, descricao FROM produtos WHERE id = ?;"
	produtoFindAllQuery = "SELECT id, cod, descricao FROM produtos LIMIT ? OFFSET ?"
)

type produtoRepositoryMysql struct {
	db *sql.DB
}

func NewProdutoRepositoryMysql(db *sql.DB) domain.ProdutoRepository {
	return &produtoRepositoryMysql{db: db}
}

func (r *produtoRepositoryMysql) Save(p *domain.Produto) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(produtoInsertQuery)
	if err != nil {
		return err
	}

	rs, err := stmt.Exec(p.Cod, p.Descricao)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}

	p.Id, err = rs.LastInsertId()
	return tx.Commit()
}

func (r *produtoRepositoryMysql) Update(p *domain.Produto) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(produtoUpdateQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(p.Cod, p.Descricao, p.Id)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *produtoRepositoryMysql) FindById(id int64) (*domain.Produto, error) {
	row := r.db.QueryRow(produtoFindByIdQuery, id)

	var prd domain.Produto
	err := row.Scan(&prd.Id, &prd.Cod, &prd.Descricao)
	if err != nil {
		return nil, err
	}

	return &prd, nil
}

func (r *produtoRepositoryMysql) FindAll(pg *core.Pagination) ([]*domain.Produto, error) {
	rows, err := r.db.Query(produtoFindAllQuery, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	var prds []*domain.Produto
	for rows.Next() {
		var prd domain.Produto
		_ = rows.Scan(&prd.Id, &prd.Cod, &prd.Descricao)
		prds = append(prds, &prd)
	}

	return prds, nil
}
