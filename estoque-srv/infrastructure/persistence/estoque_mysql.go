package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"
)

const (
	estoqueInsertQuery   = "INSERT INTO estoques (nome) VALUES (?);"
	estoqueUpdateQuery   = "UPDATE estoques SET nome = ? WHERE id = ?;"
	estoqueFindByIdQuery = "SELECT id, nome from estoques WHERE id = ?;"
	estoqueFindAllQuery  = "SELECT id, nome from estoques LIMIT ? OFFSET ?;"
)

type estoqueRepositoryMysql struct {
	db *sql.DB
	adapter *core.MysqlAdapter
}

func NewEstoqueRepositoryMysql(db *sql.DB) domain.EstoqueRepository {
	return &estoqueRepositoryMysql{
		db: db,
		adapter: core.NewMysqlAdapter(db),
	}
}

func (r *estoqueRepositoryMysql) Save(e *domain.Estoque) error {
	execute := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(e.Nome)
	}

	uid, err := r.adapter.SaveQuery(estoqueInsertQuery, execute)
	if err != nil {
		return err
	}

	e.Id = uid
	return nil
}

func (r *estoqueRepositoryMysql) Update(e *domain.Estoque) error {
	execute := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(e.Nome, e.Id)
	}

	err := r.adapter.UpdateQuery(estoqueUpdateQuery, execute)
	if err != nil {
		return err
	}

	return nil
}

func (r *estoqueRepositoryMysql) FindById(id int64) (*domain.Estoque, error) {
	row := r.db.QueryRow(estoqueFindByIdQuery, id)
	var est domain.Estoque
	err := row.Scan(&est.Id, &est.Nome)
	if err != nil {
		return nil, err
	}
	return &est, nil
}

func (r *estoqueRepositoryMysql) FindAll(pg *core.Pagination) ([]*domain.Estoque, error) {
	rows, err := r.db.Query(estoqueFindAllQuery, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	var ests []*domain.Estoque
	for rows.Next() {
		var est domain.Estoque
		rows.Scan(&est.Id, &est.Nome)
		ests = append(ests, &est)
	}
	return ests, nil
}
