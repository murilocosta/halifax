package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"

	"github.com/micro/go-micro/util/log"
)

const (
	peInsertQuery                    = "INSERT INTO produtos_estoques (estoque_id, produto_id, qtd) VALUES (?, ?, ?);"
	peUpdateQuery                    = "UPDATE produtos_estoques SET qtd = ? WHERE estoque_id = ? AND produto_id = ?"
	peFindByIdQuery                  = "SELECT id, estoque_id, produto_id, qtd FROM produtos_estoques WHERE id = ?;"
	peFindByEstoqueAndProdutoIdQuery = "SELECT id, estoque_id, produto_id, qtd FROM produtos_estoques WHERE estoque_id = ? AND produto_id = ?;"
	peFindOperacaoByIdQuery          = "SELECT e.id, e.nome, p.id, p.cod, p.descricao, sum(pe.qtd) FROM produtos_estoques AS pe INNER JOIN estoques AS e ON e.id = pe.estoque_id INNER JOIN produtos AS p ON p.id = pe.produto_id WHERE pe.estoque_id = ? GROUP BY e.id, e.nome, p.id, p.cod, p.descricao;"
	peIsAvailableQuery               = "SELECT (e.qtd >= ?), e.qtd FROM produtos_estoques WHERE estoque_id = ? AND produto_id = ?;"
)

type produtoEstoqueRepositoryMysql struct {
	db *sql.DB
}

func NewProdutoEstoqueRepositoryMysql(db *sql.DB) domain.ProdutoEstoqueRepository {
	return &produtoEstoqueRepositoryMysql{
		db: db,
	}
}

func (r *produtoEstoqueRepositoryMysql) Save(p *domain.ProdutoEstoque) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(peInsertQuery)
	if err != nil {
		return err
	}

	rs, err := stmt.Exec(p.EstoqueId, p.ProdutoId, p.Qtd)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}

	p.Id, _ = rs.LastInsertId()
	return tx.Commit()
}

func (r *produtoEstoqueRepositoryMysql) Update(p *domain.ProdutoEstoque) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(peUpdateQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(p.Qtd, p.EstoqueId, p.ProdutoId)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *produtoEstoqueRepositoryMysql) FindById(id int64) (*domain.ProdutoEstoque, error) {
	rows, err := r.db.Query(peFindByIdQuery, id)
	if err != nil {
		return nil, err
	}
	return readProdutoEstoqueFromRows(rows)
}

func (r *produtoEstoqueRepositoryMysql) FindByEstoqueAndProdutoId(estoqueId int64, produtoId int64) (*domain.ProdutoEstoque, error) {
	row := r.db.QueryRow(peFindByEstoqueAndProdutoIdQuery, estoqueId, produtoId)
	var pe domain.ProdutoEstoque
	err := row.Scan(&pe.Id, &pe.EstoqueId, &pe.ProdutoId, &pe.Qtd)
	if err != nil {
		return nil, err
	}
	return &pe, nil
}

func (r *produtoEstoqueRepositoryMysql) FindAllProdutosByEstoqueId(estoqueId int64) (*domain.OperacaoEstoque, error) {
	rows, err := r.db.Query(peFindOperacaoByIdQuery, estoqueId)
	if err != nil {
		return nil, err
	}
	op := &domain.OperacaoEstoque{}
	var e domain.Estoque
	for rows.Next() {
		var p domain.Produto
		var qtd int32
		rows.Scan(&e.Id, &e.Nome, &p.Id, &p.Cod, &p.Descricao, &qtd)
		op.AdicionarProduto(&p, qtd)
	}
	op.Estoque = &e
	return op, nil
}

func (r *produtoEstoqueRepositoryMysql) IsAvailable(estoqueId int64, produtoId int64, qtd int32) (*domain.DisponibilidadeEstoque, error) {
	row := r.db.QueryRow(peIsAvailableQuery, qtd, estoqueId, produtoId)
	var d domain.DisponibilidadeEstoque
	err := row.Scan(&d.Disponivel, &d.QtdDisponivel)
	if err != nil {
		return nil, err
	}
	return &d, nil
}

func readProdutoEstoqueFromRows(rows *sql.Rows) (*domain.ProdutoEstoque, error) {
	var pe domain.ProdutoEstoque
	for rows.Next() {
		err := rows.Scan(&pe.Id, &pe.EstoqueId, &pe.ProdutoId, &pe.Qtd)
		if err != nil {
			return nil, err
		}
	}
	return &pe, nil
}
