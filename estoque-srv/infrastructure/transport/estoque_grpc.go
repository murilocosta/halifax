package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/estoque-srv/application"
	estoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/estoque"

	"github.com/micro/go-micro/util/log"
)

type estoqueHandlerGrpc struct {
	ucase *application.EstoqueUseCase
}

func NewEstoqueHandlerGrpc(ucase *application.EstoqueUseCase) estoque.EstoqueHandler {
	return &estoqueHandlerGrpc{
		ucase: ucase,
	}
}

func (h *estoqueHandlerGrpc) Criar(ctx context.Context, req *estoque.CriarEstoqueRequest, rsp *estoque.CriarEstoqueResponse) error {
	log.Log("Received Estoque.Criar remote call")

	uid, err := h.ucase.Criar(req.Estoque.Nome)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (h *estoqueHandlerGrpc) Alterar(ctx context.Context, req *estoque.AlterarEstoqueRequest, rsp *estoque.AlterarEstoqueResponse) error {
	log.Log("Received Estoque.Alterar remote call")

	uid, err := h.ucase.Alterar(req.Estoque.Id, req.Estoque.Nome)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (h *estoqueHandlerGrpc) Listar(ctx context.Context, req *estoque.ListarEstoquesRequest, rsp estoque.Estoque_ListarStream) error {
	log.Log("Received Estoque.Listar remote call")

	items, err := h.ucase.Listar(req.Page, req.Size)
	if err != nil {
		return err
	}

	for _, item := range items {
		msg := &estoque.EstoqueMessage{
			Id:   item.Id,
			Nome: item.Nome,
		}
		_ = rsp.Send(msg)
	}

	return rsp.Close()
}
