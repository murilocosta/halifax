package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/estoque-srv/application"
	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"
	produtoEstoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto-estoque"

	"github.com/micro/go-micro/util/log"
)

type produtoEstoqueHandlerGrpc struct {
	ucase *application.ProdutoEstoqueUseCase
}

func NewProdutoEstoqueHandlerGrpc(ucase *application.ProdutoEstoqueUseCase) produtoEstoque.ProdutoEstoqueHandler {
	return &produtoEstoqueHandlerGrpc{
		ucase: ucase,
	}
}

func (h *produtoEstoqueHandlerGrpc) Adicionar(ctx context.Context, req *produtoEstoque.AdicionarProdutoEstoqueRequest, rsp *produtoEstoque.AdicionarProdutoEstoqueResponse) error {
	log.Log("Received ProdutoEstoque.Adicionar remote call")

	qtdAtual, err := h.ucase.Adicionar(
		req.ProdutoEstoque.EstoqueId,
		req.ProdutoEstoque.ProdutoId,
		req.ProdutoEstoque.Qtd,
	)
	if err != nil {
		return err
	}

	rsp.QtdAtual = qtdAtual
	return nil
}

func (h *produtoEstoqueHandlerGrpc) Remover(ctx context.Context, req *produtoEstoque.RemoverProdutoEstoqueRequest, rsp *produtoEstoque.RemoverProdutoEstoqueResponse) error {
	log.Log("Received ProdutoEstoque.Remover remote call")

	qtdAtual, err := h.ucase.Remover(
		req.ProdutoEstoque.EstoqueId,
		req.ProdutoEstoque.ProdutoId,
		req.ProdutoEstoque.Qtd,
	)
	if err != nil {
		return err
	}

	rsp.QtdAtual = qtdAtual
	return nil
}

func (h *produtoEstoqueHandlerGrpc) Listar(ctx context.Context, req *produtoEstoque.ListarProdutosEstoqueRequest, rsp *produtoEstoque.ListarProdutosEstoqueResponse) error {
	log.Log("Received ProdutoEstoque.Listar remote call")

	op, err := h.ucase.Listar(req.EstoqueId)
	if err != nil {
		return err
	}

	rsp.Estoque = convertToOperacaoEstoqueMessage(op.Estoque)
	for _, v := range op.Produtos {
		prd := convertToOperacaoProdutoMessage(v.Prod, v.Qtd)
		rsp.Produtos = append(rsp.Produtos, prd)
	}

	return nil
}

func (h *produtoEstoqueHandlerGrpc) VerificarDisponibilidade(ctx context.Context, stream produtoEstoque.ProdutoEstoque_VerificarDisponibilidadeStream) error {
	log.Log("Received ProdutoEstoque.VerificarDisponibilidade remote call")

	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}

		disp, err := h.ucase.VerificarDisponibilidade(req.EstoqueId, req.ProdutoId, req.Qtd)
		if err != nil {
			return err
		}

		err = stream.Send(&produtoEstoque.ProdutoDisponibilidadeMessage{
			EstoqueId:     req.EstoqueId,
			ProdutoId:     req.ProdutoId,
			Disponivel:    disp.Disponivel,
			QtdDisponivel: disp.QtdDisponivel,
		})

		if err != nil {
			return err
		}
	}
}

func convertToOperacaoEstoqueMessage(est *domain.Estoque) *produtoEstoque.LstEstoqueMessage {
	return &produtoEstoque.LstEstoqueMessage{
		Id:   est.Id,
		Nome: est.Nome,
	}
}

func convertToOperacaoProdutoMessage(prd *domain.Produto, qtd int32) *produtoEstoque.LstProdutoMessage {
	return &produtoEstoque.LstProdutoMessage{
		Id:        prd.Id,
		Cod:       prd.Cod,
		Descricao: prd.Descricao,
		Qtd:       qtd,
	}
}
