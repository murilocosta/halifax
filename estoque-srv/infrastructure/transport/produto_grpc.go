package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/estoque-srv/application"
	produto "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto"

	"github.com/micro/go-micro/util/log"
)

type produtoHandlerGrpc struct {
	ucase *application.ProdutoUseCase
}

func NewProdutoHandlerGrpc(ucase *application.ProdutoUseCase) produto.ProdutoHandler {
	return &produtoHandlerGrpc{
		ucase: ucase,
	}
}

func (h *produtoHandlerGrpc) Criar(ctx context.Context, req *produto.CriarProdutoRequest, rsp *produto.CriarProdutoResponse) error {
	log.Log("Received Produto.Criar remote call")

	uid, err := h.ucase.Criar(req.Produto.Cod, req.Produto.Descricao)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (h *produtoHandlerGrpc) Alterar(ctx context.Context, req *produto.AlterarProdutoRequest, rsp *produto.AlterarProdutoResponse) error {
	log.Log("Received Produto.Alterar remote call")

	uid, err := h.ucase.Alterar(req.Produto.Id, req.Produto.Cod, req.Produto.Descricao)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (h *produtoHandlerGrpc) Listar(ctx context.Context, req *produto.ListarProdutosRequest, rsp produto.Produto_ListarStream) error {
	log.Log("Received Produto.Listar remote call")

	prds, err := h.ucase.Listar(req.Page, req.Size)
	if err != nil {
		return err
	}

	for _, prd := range prds {
		msg := &produto.ProdutoMessage{
			Id:        prd.Id,
			Cod:       prd.Cod,
			Descricao: prd.Descricao,
		}
		_ = rsp.Send(msg)
	}

	return rsp.Close()
}
