package main

import (
	"database/sql"
	"os"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-srv/application"
	"bitbucket.org/murilocosta/halifax/estoque-srv/infrastructure/persistence"
	"bitbucket.org/murilocosta/halifax/estoque-srv/infrastructure/transport"
	estoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/estoque"
	produto "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto"
	produtoEstoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto-estoque"
	"bitbucket.org/murilocosta/halifax/estoque-srv/subscriber"

	_ "github.com/go-sql-driver/mysql"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"github.com/micro/go-micro/util/log"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	dsl, _ := core.NewDataSourceLocation(
		config.DatabaseHost,
		config.DatabaseUser,
		config.DatabasePass,
		"halifax_estoque",
	)
	db, err := sql.Open(config.DatabaseDriver, dsl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.estoque"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	s := service.Server()
	registerEstoqueHandler(db, s)
	registerProdutoHandler(db, s)
	registerProdutoEstoqueHandler(db, s)

	// Register Struct as Subscriber
	micro.RegisterSubscriber("go.micro.srv.estoque", service.Server(), new(subscriber.Estoque))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func registerEstoqueHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewEstoqueRepositoryMysql(db)
	ucase := application.NewEstoqueUseCase(repo)
	handler := transport.NewEstoqueHandlerGrpc(ucase)
	return estoque.RegisterEstoqueHandler(s, handler)
}

func registerProdutoHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewProdutoRepositoryMysql(db)
	ucase := application.NewProdutoUseCase(repo)
	handler := transport.NewProdutoHandlerGrpc(ucase)
	return produto.RegisterProdutoHandler(s, handler)
}

func registerProdutoEstoqueHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewProdutoEstoqueRepositoryMysql(db)
	ucase := application.NewProdutoEstoqueUseCase(repo)
	handler := transport.NewProdutoEstoqueHandlerGrpc(ucase)
	return produtoEstoque.RegisterProdutoEstoqueHandler(s, handler)
}
