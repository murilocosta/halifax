package application

import (
	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"
)

type ProdutoUseCase struct {
	repo domain.ProdutoRepository
}

func NewProdutoUseCase(repo domain.ProdutoRepository) *ProdutoUseCase {
	return &ProdutoUseCase{
		repo:repo,
	}
}

func (ucase *ProdutoUseCase) Criar(cod string, descricao string) (int64, error) {
	p := domain.NewProduto(cod, descricao)
	err := ucase.repo.Save(p)
	if err != nil {
		return -1, err
	}
	return p.Id, nil
}

func (ucase *ProdutoUseCase) Alterar(id int64, cod string, descricao string) (int64, error) {
	p, err := ucase.repo.FindById(id)
	if err != nil {
		return -1, err
	}
	p.Update(cod, descricao)
	err = ucase.repo.Update(p)
	if err != nil {
		return -1, err
	}
	return p.Id, nil
}

func (ucase *ProdutoUseCase) Listar(page int32, size int32) ([]*domain.Produto, error) {
	pg := core.NewPagination(page, size)
	prds, err := ucase.repo.FindAll(pg)
	if err != nil {
		return nil, err
	}
	return prds, nil
}
