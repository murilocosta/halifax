package application

import (
	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"
)

type EstoqueUseCase struct {
	repo domain.EstoqueRepository
}

func NewEstoqueUseCase(repo domain.EstoqueRepository) *EstoqueUseCase {
	return &EstoqueUseCase{
		repo: repo,
	}
}

func (ucase *EstoqueUseCase) Criar(nome string) (int64, error) {
	e := domain.NewEstoque(nome)
	err := ucase.repo.Save(e)
	if err != nil {
		return -1, err
	}
	return e.Id, nil
}

func (ucase *EstoqueUseCase) Alterar(id int64, nome string) (int64, error) {
	e, err := ucase.repo.FindById(id)
	if err != nil {
		return -1, err
	}
	e.Update(nome)
	if err = ucase.repo.Update(e); err != nil {
		return -1, err
	}
	return e.Id, nil
}

func (ucase *EstoqueUseCase) Listar(page int32, size int32) ([]*domain.Estoque, error) {
	pg := core.NewPagination(page, size)
	list, err := ucase.repo.FindAll(pg)
	if err != nil {
		return nil, err
	}
	return list, nil
}
