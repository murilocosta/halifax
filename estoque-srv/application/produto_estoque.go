package application

import (
	"database/sql"
	"errors"

	"bitbucket.org/murilocosta/halifax/estoque-srv/domain"
)

type ProdutoEstoqueUseCase struct {
	repo domain.ProdutoEstoqueRepository
}

func NewProdutoEstoqueUseCase(repo domain.ProdutoEstoqueRepository) *ProdutoEstoqueUseCase {
	return &ProdutoEstoqueUseCase{
		repo: repo,
	}
}

func (u *ProdutoEstoqueUseCase) Adicionar(estoqueId int64, produtoId int64, qtd int32) (int32, error) {
	pe, err := u.repo.FindByEstoqueAndProdutoId(estoqueId, produtoId)
	if err == sql.ErrNoRows {
		pe = domain.NewProdutoEstoque(estoqueId, produtoId, qtd)
		err = u.repo.Save(pe)
	} else if err != nil {
		return -1, err
	} else {
		pe.Adicionar(qtd)

		err = u.repo.Update(pe)
		if err != nil {
			return -1, err
		}
	}

	return pe.Qtd, nil
}

func (u *ProdutoEstoqueUseCase) Remover(estoqueId int64, produtoId int64, qtd int32) (int32, error) {
	pe, err := u.repo.FindByEstoqueAndProdutoId(estoqueId, produtoId)
	if err != nil {
		return -1, err
	}

	if pe.Qtd < qtd {
		return pe.Qtd, errors.New("Não há itens suficientes")
	}

	pe.Remover(qtd)
	err = u.repo.Update(pe)
	if err != nil {
		return -1, err
	}
	return pe.Qtd, nil
}

func (u *ProdutoEstoqueUseCase) Listar(estoqueId int64) (*domain.OperacaoEstoque, error) {
	return u.repo.FindAllProdutosByEstoqueId(estoqueId)
}

func (u *ProdutoEstoqueUseCase) VerificarDisponibilidade(estoqueId int64, produtoId int64, qtd int32) (*domain.DisponibilidadeEstoque, error) {
	return u.repo.IsAvailable(estoqueId, produtoId, qtd)
}
