# Financeiro Service

This is the Financeiro service

Generated with

```
micro new bitbucket.org/murilocosta/halifax/financeiro-api --namespace=go.micro --type=api
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: go.micro.api.financeiro
- Type: api
- Alias: financeiro

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./financeiro-api
```

Build a docker image
```
make docker
```