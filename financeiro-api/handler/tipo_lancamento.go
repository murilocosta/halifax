package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-api/client"
	tipoLancamentoApi "bitbucket.org/murilocosta/halifax/financeiro-api/proto/financeiro"
	tipoLancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/tipo-lancamento"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type tipoLancamentoHandlerGateway struct{}

func NewTipoLancamentoHandlerGateway() tipoLancamentoApi.TipoLancamentoHandler {
	return &tipoLancamentoHandlerGateway{}
}

func (*tipoLancamentoHandlerGateway) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received TipoLancamento.Criar request")

	// extract the client from the context
	financeiroClient, ok := client.TipoLancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	var payload tipoLancamento.TipoLancamentoMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	response, err := financeiroClient.Criar(ctx, &tipoLancamento.CriarTipoLancamentoRequest{
		Tipo: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*tipoLancamentoHandlerGateway) Alterar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received TipoLancamento.Alterar request")

	// extract the client from the context
	financeiroClient, ok := client.TipoLancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	var payload tipoLancamento.TipoLancamentoMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Alterar(ctx, &tipoLancamento.AlterarTipoLancamentoRequest{
		Tipo: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*tipoLancamentoHandlerGateway) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received TipoLancamento.Listar request")

	// extract the client from the context
	financeiroClient, ok := client.TipoLancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := financeiroClient.Listar(ctx, &tipoLancamento.ListarTipoLancamentosRequest{
		Page: page,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	var items []*tipoLancamento.TipoLancamentoMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
