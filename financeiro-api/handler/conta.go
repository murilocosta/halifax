package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/core"

	"bitbucket.org/murilocosta/halifax/financeiro-api/client"
	contaApi "bitbucket.org/murilocosta/halifax/financeiro-api/proto/financeiro"
	conta "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/conta"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type contaHandlerGateway struct{}

func NewContaHandlerGateway() contaApi.ContaHandler {
	return &contaHandlerGateway{}
}

func (*contaHandlerGateway) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Conta.Criar request")

	// extract the client from the context
	financeiroClient, ok := client.ContaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	var payload conta.ContaMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Criar(ctx, &conta.CriarContaRequest{
		Conta: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*contaHandlerGateway) Alterar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Conta.Alterar request")

	// extract the client from the context
	financeiroClient, ok := client.ContaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	var payload conta.ContaMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Alterar(ctx, &conta.AlterarContaRequest{
		Conta: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*contaHandlerGateway) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Conta.Listar request")

	// extract the client from the context
	financeiroClient, ok := client.ContaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := financeiroClient.Listar(ctx, &conta.ListarContasRequest{
		Page: page,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	var items []*conta.ContaMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
