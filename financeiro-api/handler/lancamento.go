package handler

import (
	"bitbucket.org/murilocosta/halifax/core"
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/financeiro-api/client"
	lancamentoApi "bitbucket.org/murilocosta/halifax/financeiro-api/proto/financeiro"
	lancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/lancamento"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type lancamentoHandlerGateway struct{}

func NewLancamentoHandlerGateway() lancamentoApi.LancamentoHandler {
	return &lancamentoHandlerGateway{}
}

func (*lancamentoHandlerGateway) Registrar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Lancamento.Registrar request")

	// extract the client from the context
	financeiroClient, ok := client.LancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	// make request
	var payload lancamento.RegistrarLancamentoRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Registrar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*lancamentoHandlerGateway) Atualizar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Lancamento.Atualizar request")

	// extract the client from the context
	financeiroClient, ok := client.LancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	var payload lancamento.AtualizarLancamentoRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Atualizar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*lancamentoHandlerGateway) Estornar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Lancamento.Estornar request")

	// extract the client from the context
	financeiroClient, ok := client.LancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	var payload lancamento.EstornarLancamentoRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Estornar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*lancamentoHandlerGateway) Agendar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Lancamento.Agendar request")

	// extract the client from the context
	financeiroClient, ok := client.LancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	var payload lancamento.AgendarLancamentoRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	response, err := financeiroClient.Agendar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*lancamentoHandlerGateway) ListarConta(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Lancamento.ListarConta request")

	// extract the client from the context
	financeiroClient, ok := client.LancamentoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.financeiro", "financeiro client not found")
	}

	contaId := core.ExtractValueFromRequestAsInt64(req.Get["contaId"], 0)
	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := financeiroClient.ListarConta(ctx, &lancamento.ListarLancamentosContaRequest{
		ContaId: contaId,
		Page:    page,
		Size:    size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.financeiro", err.Error())
	}

	var items []*lancamento.LancamentoContaMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.financeiro", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
