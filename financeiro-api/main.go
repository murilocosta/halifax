package main

import (
	"os"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-api/client"
	"bitbucket.org/murilocosta/halifax/financeiro-api/handler"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"

	financeiro "bitbucket.org/murilocosta/halifax/financeiro-api/proto/financeiro"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.api.financeiro"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init(
		// create wrap for the Financeiro srv client
		micro.WrapHandler(client.ContaWrapper(service)),
		micro.WrapHandler(client.LancamentoWrapper(service)),
		micro.WrapHandler(client.TipoLancamentoWrapper(service)),
	)

	// Register Handler
	financeiro.RegisterContaHandler(service.Server(), handler.NewContaHandlerGateway())
	financeiro.RegisterLancamentoHandler(service.Server(), handler.NewLancamentoHandlerGateway())
	financeiro.RegisterTipoLancamentoHandler(service.Server(), handler.NewTipoLancamentoHandlerGateway())

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
