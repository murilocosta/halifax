package client

import (
	"context"

	lancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/lancamento"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type lancamentoKey struct{}

// FromContext retrieves the client from the Context
func LancamentoFromContext(ctx context.Context) (lancamento.LancamentoService, bool) {
	c, ok := ctx.Value(lancamentoKey{}).(lancamento.LancamentoService)
	return c, ok
}

// Client returns a wrapper for the FinanceiroClient
func LancamentoWrapper(service micro.Service) server.HandlerWrapper {
	client := lancamento.NewLancamentoService("go.micro.srv.financeiro", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, lancamentoKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
