package client

import (
	"context"

	tipoLancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/tipo-lancamento"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type tipoLancamentoKey struct{}

// FromContext retrieves the client from the Context
func TipoLancamentoFromContext(ctx context.Context) (tipoLancamento.TipoLancamentoService, bool) {
	c, ok := ctx.Value(tipoLancamentoKey{}).(tipoLancamento.TipoLancamentoService)
	return c, ok
}

// Client returns a wrapper for the FinanceiroClient
func TipoLancamentoWrapper(service micro.Service) server.HandlerWrapper {
	client := tipoLancamento.NewTipoLancamentoService("go.micro.srv.financeiro", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, tipoLancamentoKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
