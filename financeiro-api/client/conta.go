package client

import (
	"context"

	conta "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/conta"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type contaKey struct{}

// FromContext retrieves the client from the Context
func ContaFromContext(ctx context.Context) (conta.ContaService, bool) {
	c, ok := ctx.Value(contaKey{}).(conta.ContaService)
	return c, ok
}

// Client returns a wrapper for the FinanceiroClient
func ContaWrapper(service micro.Service) server.HandlerWrapper {
	client := conta.NewContaService("go.micro.srv.financeiro", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, contaKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
