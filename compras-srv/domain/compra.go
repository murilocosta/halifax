package domain

import (
	"time"

	"bitbucket.org/murilocosta/halifax/core"
)

type Compra struct {
	Id         int64
	Cod        string
	TotalBruto float64
	Desconto   float64
	Emissao    time.Time
}

func NewCompra(cod string, totalBruto float64, desconto float64, emissao time.Time) *Compra {
	return &Compra{
		Cod:        cod,
		TotalBruto: totalBruto,
		Desconto:   desconto,
		Emissao:    emissao,
	}
}

func (c *Compra) Update(cod string) {
	c.Cod = cod
}

type ProdutoCompra struct {
	Id       int64
	CompraId int64
	ProdId   int64
	ValUnit  float64
	Qtd      int32
}

func NewProdutoCompra(prodId int64, valUnit float64, qtd int32) *ProdutoCompra {
	return &ProdutoCompra{
		ProdId:  prodId,
		ValUnit: valUnit,
		Qtd:     qtd,
	}
}

func (p *ProdutoCompra) Update(compraId int64) {
	p.CompraId = compraId
}

type OperacaoCompra struct {
	Compra *Compra
	Itens  []*ProdutoCompra
}

type CompraRepository interface {
	Save(c *OperacaoCompra) error
	Update(c *Compra) error
	FindById(id int64) (*Compra, error)
	FindOperacaoById(compraId int64) (*OperacaoCompra, error)
	FindAll(p *core.Pagination) ([]*Compra, error)
}
