package application

import (
	"bitbucket.org/murilocosta/halifax/compras-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"
)

type CompraUseCase struct {
	compraRepo domain.CompraRepository
}

func NewCompraUseCase(compraRepo domain.CompraRepository) *CompraUseCase {
	return &CompraUseCase{
		compraRepo: compraRepo,
	}
}

func (ucase *CompraUseCase) Criar(unit *domain.OperacaoCompra) (int64, error) {
	err := ucase.compraRepo.Save(unit)
	if err != nil {
		return -1, err
	}
	return unit.Compra.Id, nil
}

func (ucase *CompraUseCase) Alterar(id int64, cod string) (int64, error) {
	c, err := ucase.compraRepo.FindById(id)
	if err != nil {
		return -1, err
	}
	c.Update(cod)
	err = ucase.compraRepo.Update(c)
	if err != nil {
		return -1, err
	}
	return c.Id, nil
}

func (ucase *CompraUseCase) Buscar(id int64) (*domain.OperacaoCompra, error) {
	return ucase.compraRepo.FindOperacaoById(id)
}

func (ucase *CompraUseCase) Listar(pg *core.Pagination) ([]*domain.Compra, error) {
	return ucase.compraRepo.FindAll(pg)
}
