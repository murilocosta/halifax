package main

import (
	"database/sql"
	"os"

	"bitbucket.org/murilocosta/halifax/compras-srv/application"
	"bitbucket.org/murilocosta/halifax/compras-srv/infrastructure/persistence"
	"bitbucket.org/murilocosta/halifax/compras-srv/infrastructure/transport"
	compra "bitbucket.org/murilocosta/halifax/compras-srv/proto/compra"
	"bitbucket.org/murilocosta/halifax/core"

	_ "github.com/go-sql-driver/mysql"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"github.com/micro/go-micro/util/log"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	dsl, _ := core.NewDataSourceLocation(
		config.DatabaseHost,
		config.DatabaseUser,
		config.DatabasePass,
		"halifax_compras",
	)

	db, err := sql.Open(config.DatabaseDriver, dsl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.compras"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	s := service.Server()
	RegisterCompraHandler(db, s)

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func RegisterCompraHandler(db *sql.DB, s server.Server) {
	repo := persistence.NewCompraRepositoryMysql(db)
	ucase := application.NewCompraUseCase(repo)
	handler := transport.NewCompraHandlerGrpc(ucase)
	if err := compra.RegisterCompraHandler(s, handler); err != nil {
		log.Fatal(err)
	}
}
