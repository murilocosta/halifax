package persistence

import (
	"database/sql"
	"strings"

	"bitbucket.org/murilocosta/halifax/compras-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro/util/log"
)

const (
	insertCompraQuery        = "INSERT INTO compras (cod, total_bruto, desconto, emissao) VALUES (?, ?, ?, ?);"
	insertProdutoQuery       = "INSERT INTO compra_produtos (compra_id, prod_id, val_unit, qtd) VALUES "
	insertProdutoQueryParams = "(?, ?, ?, ?),"
	updateQuery              = "UPDATE compras SET cod = ? WHERE id = ?;"
	findByIdQuery            = "SELECT id, cod, total_bruto, desconto, emissao FROM compras WHERE id = ?;"
	findByOperacaoIdQuery    = "SELECT c.id, c.cod, c.total_bruto, c.desconto, c.emissao, p.id, p.compra_id, p.prod_id, p.val_unit, p.qtd FROM compras AS c INNER JOIN compra_produtos AS p ON p.compra_id = c.id WHERE c.id = ?"
	findAllQuery             = "SELECT id, cod, total_bruto, desconto, emissao FROM compras LIMIT ? OFFSET ?"
)

type compraRepositoryMysql struct {
	db *sql.DB
}

func NewCompraRepositoryMysql(db *sql.DB) domain.CompraRepository {
	return &compraRepositoryMysql{db: db}
}

func (r *compraRepositoryMysql) Save(c *domain.OperacaoCompra) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	stmt, _ := tx.Prepare(insertCompraQuery)
	rs, err := stmt.Exec(c.Compra.Cod, c.Compra.TotalBruto, c.Compra.Desconto, c.Compra.Emissao)
	if err != nil {
		return tx.Rollback()
	}
	c.Compra.Id, _ = rs.LastInsertId()

	batchQuery := insertProdutoQuery
	var params []interface{}
	for _, v := range c.Itens {
		batchQuery += insertProdutoQueryParams
		params = append(params, c.Compra.Id, v.ProdId, v.ValUnit, v.Qtd)
	}
	batchQuery = strings.TrimSuffix(batchQuery, ",")

	stmt, err = tx.Prepare(batchQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(params)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *compraRepositoryMysql) Update(c *domain.Compra) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	stmt, _ := tx.Prepare(updateQuery)
	_, err = stmt.Exec(c.Cod, c.Id)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *compraRepositoryMysql) FindById(id int64) (*domain.Compra, error) {
	rows, err := r.db.Query(findByIdQuery, id)
	if err != nil {
		return nil, err
	}
	var cmp domain.Compra
	for rows.Next() {
		readCompraFromRows(rows, &cmp)
	}
	return &cmp, nil
}

func (r *compraRepositoryMysql) FindOperacaoById(compraId int64) (*domain.OperacaoCompra, error) {
	rows, err := r.db.Query(findByOperacaoIdQuery, compraId)
	if err != nil {
		return nil, err
	}

	var cmp domain.Compra
	var prds []*domain.ProdutoCompra
	for rows.Next() {
		var prd domain.ProdutoCompra
		readOperacaoFromRows(rows, &cmp, &prd)
		prds = append(prds, &prd)
	}

	op := &domain.OperacaoCompra{
		Compra: &cmp,
		Itens:  prds,
	}
	return op, nil
}

func (r *compraRepositoryMysql) FindAll(p *core.Pagination) ([]*domain.Compra, error) {
	rows, err := r.db.Query(findAllQuery, p.GetSize(), p.GetPage())
	if err != nil {
		return nil, err
	}
	var cmps []*domain.Compra
	for rows.Next() {
		var cmp domain.Compra
		readCompraFromRows(rows, &cmp)
		cmps = append(cmps, &cmp)
	}
	return nil, nil
}

func readCompraFromRows(rows *sql.Rows, cmp *domain.Compra) error {
	return rows.Scan(
		cmp.Id,
		cmp.Cod,
		cmp.TotalBruto,
		cmp.Desconto,
		cmp.Emissao,
	)
}

func readOperacaoFromRows(rows *sql.Rows, cmp *domain.Compra, prd *domain.ProdutoCompra) error {
	return rows.Scan(
		cmp.Id,
		cmp.Cod,
		cmp.TotalBruto,
		cmp.Desconto,
		cmp.Emissao,
		prd.Id,
		prd.CompraId,
		prd.ProdId,
		prd.ValUnit,
		prd.Qtd,
	)
}
