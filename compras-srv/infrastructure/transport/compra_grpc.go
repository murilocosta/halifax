package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/compras-srv/application"
	"bitbucket.org/murilocosta/halifax/compras-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"

	compra "bitbucket.org/murilocosta/halifax/compras-srv/proto/compra"
)

type compraHandlerGrpc struct {
	ucase *application.CompraUseCase
}

func NewCompraHandlerGrpc(ucase *application.CompraUseCase) compra.CompraHandler {
	return &compraHandlerGrpc{
		ucase: ucase,
	}
}

func (h *compraHandlerGrpc) Criar(ctx context.Context, req *compra.CriarRequest, rsp *compra.CriarResponse) error {
	emissao := core.ConvertFromTimestamp(req.Compra.Emissao)
	comp := domain.NewCompra(req.Compra.Cod, req.Compra.TotalBruto, req.Compra.Desconto, emissao)

	var itens []*domain.ProdutoCompra
	for _, v := range req.Itens {
		item := domain.NewProdutoCompra(v.ProdId, v.ValUnit, v.Qtd)
		itens = append(itens, item)
	}

	cId, err := h.ucase.Criar(&domain.OperacaoCompra{
		Compra: comp,
		Itens:  itens,
	})

	if err != nil {
		return err
	}
	rsp.Id = cId
	return nil
}

func (h *compraHandlerGrpc) Alterar(ctx context.Context, req *compra.AlterarRequest, rsp *compra.AlterarResponse) error {
	cId, err := h.ucase.Alterar(req.Id, req.Cod)
	if err != nil {
		return err
	}
	rsp.Id = cId
	return nil
}

func (h *compraHandlerGrpc) Buscar(ctx context.Context, req *compra.BuscarRequest, rsp *compra.BuscarResponse) error {
	o, err := h.ucase.Buscar(req.Id)
	if err != nil {
		return err
	}
	convertToCompraMessage(o.Compra, rsp.Compra)
	for _, v := range o.Itens {
		var prod compra.ProdutoCompraMessage
		convertToCompraProdutoMessage(v, &prod)
		rsp.Itens = append(rsp.Itens, &prod)
	}
	return nil
}

func (h *compraHandlerGrpc) Listar(ctx context.Context, req *compra.ListarRequest, rsp compra.Compra_ListarStream) error {
	pg := core.NewPagination(req.Page, req.Size)
	compras, err := h.ucase.Listar(pg)
	if err != nil {
		return err
	}
	for _, c := range compras {
		var message compra.CompraMessage
		convertToCompraMessage(c, &message)
		rsp.Send(&message)
	}
	return rsp.Close()
}

func convertToCompraMessage(c *domain.Compra, message *compra.CompraMessage) {
	message.Id = c.Id
	message.Desconto = c.Desconto
	message.TotalBruto = c.TotalBruto
	message.Cod = c.Cod
	message.Emissao = core.ConvertToTimestamp(c.Emissao)
}

func convertToCompraProdutoMessage(p *domain.ProdutoCompra, message *compra.ProdutoCompraMessage) {
	message.Id = p.Id
	message.CompraId = p.CompraId
	message.ProdId = p.ProdId
	message.ValUnit = p.ValUnit
	message.Qtd = p.Qtd
}
