CREATE DATABASE IF NOT EXISTS halifax_caixa;

USE halifax_caixa;

CREATE TABLE caixas
(
  id   INTEGER AUTO_INCREMENT,
  nome VARCHAR(120) NOT NULL,
  CONSTRAINT pk_caixa PRIMARY KEY (id)
);

CREATE TABLE caixa_turnos
(
  id          INTEGER AUTO_INCREMENT,
  caixa_id    INTEGER  NOT NULL,
  abertura    DATETIME NOT NULL,
  fechamento  DATETIME,
  fundo_caixa NUMERIC(10, 2),
  saldo_final NUMERIC(12, 2),
  CONSTRAINT pk_caixaturno PRIMARY KEY (id),
  CONSTRAINT fk_caixaturno_caixa FOREIGN KEY (caixa_id) REFERENCES caixas (id)
);

CREATE TABLE registros
(
  id             INTEGER AUTO_INCREMENT,
  caixa_turno_id INTEGER        NOT NULL,
  valor          NUMERIC(12, 2) NOT NULL,
  descricao      VARCHAR(80),
  emissao        DATETIME       NOT NULL,
  CONSTRAINT pk_registro PRIMARY KEY (id),
  CONSTRAINT fk_registro_caixaturno FOREIGN KEY (caixa_turno_id) REFERENCES caixa_turnos (id)
);
