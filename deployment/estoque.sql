CREATE DATABASE IF NOT EXISTS halifax_estoque;

USE halifax_estoque;

CREATE TABLE estoques
(
  id   INTEGER AUTO_INCREMENT,
  nome VARCHAR(130) NOT NULL,
  CONSTRAINT pk_estoque PRIMARY KEY (id)
);

CREATE TABLE produtos
(
  id        INTEGER AUTO_INCREMENT,
  cod       VARCHAR(32),
  descricao VARCHAR(120) NOT NULL,
  CONSTRAINT pk_produto PRIMARY KEY (id)
);

CREATE TABLE produtos_estoques
(
  id         INTEGER AUTO_INCREMENT,
  estoque_id INTEGER NOT NULL,
  produto_id INTEGER NOT NULL,
  qtd        INTEGER NOT NULL,
  CONSTRAINT pk_produtoestoque PRIMARY KEY (id),
  CONSTRAINT fk_produtoestoque_estoque FOREIGN KEY (estoque_id) REFERENCES estoques (id),
  CONSTRAINT fk_produtoestoque_produto FOREIGN KEY (produto_id) REFERENCES produtos (id)
);
