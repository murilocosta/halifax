CREATE DATABASE IF NOT EXISTS halifax_financeiro;

use halifax_financeiro;

CREATE TABLE contas
(
  id   INTEGER AUTO_INCREMENT,
  nome VARCHAR(120) NOT NULL,
  CONSTRAINT pk_conta PRIMARY KEY (id)
);

CREATE TABLE tipos_lancamento
(
  id   INTEGER AUTO_INCREMENT,
  nome VARCHAR(80) NOT NULL,
  CONSTRAINT pk_tipolancamento PRIMARY KEY (id)
);

CREATE TABLE lancamentos
(
  id                 INTEGER AUTO_INCREMENT,
  conta_id           INTEGER        NOT NULL,
  tipo_lancamento_id INTEGER        NOT NULL,
  valor              NUMERIC(12, 2) NOT NULL,
  descricao          VARCHAR(150),
  emissao            DATETIME       NOT NULL,
  registroId         INTEGER        NOT NULL,
  CONSTRAINT pk_lancamento PRIMARY KEY (id),
  CONSTRAINT fk_lancamento_conta FOREIGN KEY (conta_id) REFERENCES contas (id),
  CONSTRAINT fk_lancamento_tipolancamento FOREIGN KEY (tipo_lancamento_id) REFERENCES tipos_lancamento (id)
);

CREATE TABLE lancamentos_futuros
(
  id                         INTEGER AUTO_INCREMENT,
  conta_destino_id           INTEGER        NOT NULL,
  tipo_lancamento_destino_id INTEGER        NOT NULL,
  valor                      NUMERIC(12, 2) NOT NULL,
  descricao                  VARCHAR(150),
  validade                   DATETIME       NOT NULL,
  emissao                    DATETIME       NOT NULL,
  registroId                 INTEGER        NOT NULL,
  CONSTRAINT pk_lancamentofuturo PRIMARY KEY (id),
  CONSTRAINT fk_lancamentofuturo_conta FOREIGN KEY (conta_destino_id) REFERENCES contas (id),
  CONSTRAINT fk_lancamentofuturo_tipolancamento FOREIGN KEY (tipo_lancamento_destino_id) REFERENCES tipos_lancamento (id)
);

CREATE TABLE lancamentos_estornos
(
  id         INTEGER AUTO_INCREMENT,
  origem_id  INTEGER  NOT NULL,
  destino_id INTEGER  NOT NULL,
  emissao    DATETIME NOT NULL,
  CONSTRAINT pk_lancamentoestorno PRIMARY KEY (id),
  CONSTRAINT fk_lancamentoestorno_origem FOREIGN KEY (origem_id) REFERENCES lancamentos (id),
  CONSTRAINT fk_lancamentoestorno_destino FOREIGN KEY (destino_id) REFERENCES lancamentos (id)
);
