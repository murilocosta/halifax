CREATE DATABASE IF NOT EXISTS halifax_vendas;

USE halifax_vendas;

CREATE TABLE vendas
(
  id          INTEGER AUTO_INCREMENT,
  cod         VARCHAR(126),
  total_bruto NUMERIC(12, 2) NOT NULL,
  desconto    NUMERIC(10, 2),
  emissao     DATETIME       NOT NULL,
  CONSTRAINT pk_venda PRIMARY KEY (id)
);

CREATE TABLE venda_produtos
(
  id        INTEGER AUTO_INCREMENT,
  venda_id INTEGER        NOT NULL,
  prod_id   INTEGER        NOT NULL,
  val_unit  NUMERIC(10, 2) NOT NULL,
  qtd       INTEGER        NOT NULL,
  CONSTRAINT pk_vendaprodutos PRIMARY KEY (id),
  CONSTRAINT fk_vendaprodutos_venda FOREIGN KEY (venda_id) REFERENCES vendas (id)
);
