CREATE DATABASE IF NOT EXISTS halifax_compras;

USE halifax_compras;

CREATE TABLE compras
(
  id          INTEGER AUTO_INCREMENT,
  cod         VARCHAR(126),
  total_bruto NUMERIC(12, 2) NOT NULL,
  desconto    NUMERIC(10, 2),
  emissao     DATETIME       NOT NULL,
  CONSTRAINT pk_compra PRIMARY KEY (id)
);

CREATE TABLE compra_produtos
(
  id        INTEGER AUTO_INCREMENT,
  compra_id INTEGER        NOT NULL,
  prod_id   INTEGER        NOT NULL,
  val_unit  NUMERIC(10, 2) NOT NULL,
  qtd       INTEGER        NOT NULL,
  CONSTRAINT pk_compraprodutos PRIMARY KEY (id),
  CONSTRAINT fk_compraprodutos_compra FOREIGN KEY (compra_id) REFERENCES compras (id)
);
