package subscriber

import (
	"context"
	"github.com/micro/go-micro/util/log"
)

type Caixa struct{}

func (e *Caixa) Handle(ctx context.Context, msg string) error {
	log.Log("Handler Received message: ", msg)
	return nil
}
