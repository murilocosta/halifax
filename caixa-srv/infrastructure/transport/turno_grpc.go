package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/caixa-srv/application"
	turno "bitbucket.org/murilocosta/halifax/caixa-srv/proto/turno"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro/util/log"
)

type turnoHandlerGrpc struct {
	ucase *application.TurnoUseCase
}

func NewTurnoHandlerGrpc(ucase *application.TurnoUseCase) turno.TurnoHandler {
	return &turnoHandlerGrpc{
		ucase: ucase,
	}
}

func (h *turnoHandlerGrpc) Iniciar(ctx context.Context, req *turno.IniciarTurnoRequest, rsp *turno.IniciarTurnoResponse) error {
	log.Log("Received CaixaTurno.Iniciar remote call")

	uid, err := h.ucase.Iniciar(req.CaixaId, req.FundoCaixa)
	if err != nil {
		return err
	}
	rsp.TurnoId = uid
	return nil
}

func (h *turnoHandlerGrpc) Finalizar(ctx context.Context, req *turno.FinalizarTurnoRequest, rsp *turno.FinalizarTurnoResponse) error {
	log.Log("Received CaixaTurno.Finalizar remote call")

	t, err := h.ucase.Finalizar(req.TurnoId)
	if err != nil {
		return err
	}
	rsp.TurnoId = t.Id
	rsp.SaldoFinal = t.SaldoFinal
	return nil
}

func (h *turnoHandlerGrpc) ListarPorCaixa(ctx context.Context, req *turno.ListarPorCaixaRequest, rsp turno.Turno_ListarPorCaixaStream) error {
	log.Log("Received CaixaTurno.ListarPorCaixa remote call")

	turnos, err := h.ucase.ListarPorCaixa(req.CaixaId, req.Page, req.Size)
	if err != nil {
		return err
	}
	for _, t := range turnos {
		_ = rsp.Send(&turno.TurnoMessage{
			Id:         t.Id,
			SaldoFinal: t.SaldoFinal,
			CaixaId:    t.CaixaId,
			FundoCaixa: t.FundoCaixa,
			Abertura:   core.ConvertToTimestamp(t.Abertura),
			Fechamento: core.ConvertToTimestamp(t.Fechamento),
		})
	}
	return rsp.Close()
}
