package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/caixa-srv/application"
	registro "bitbucket.org/murilocosta/halifax/caixa-srv/proto/registro"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro/util/log"
)

type registroHandlerGrpc struct {
	ucase *application.RegistroUseCase
}

func NewRegistroHandlerGrpc(ucase *application.RegistroUseCase) registro.RegistroHandler {
	return &registroHandlerGrpc{
		ucase: ucase,
	}
}

func (r *registroHandlerGrpc) Criar(ctx context.Context, req *registro.CriarRegistroRequest, rsp *registro.CriarRegistroResponse) error {
	log.Log("Received Registro.Criar remote call")

	emissao := core.ConvertFromTimestamp(req.Registro.Emissao)
	uid, err := r.ucase.Criar(
		req.Registro.CaixaTurnoId,
		req.Registro.Valor,
		req.Registro.Descricao,
		emissao,
	)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (r *registroHandlerGrpc) Atualizar(ctx context.Context, req *registro.AtualizarRegistroRequest, rsp *registro.AtualizarRegistroResponse) error {
	log.Log("Received Registro.Atualizar remote call")

	uid, err := r.ucase.Atualizar(req.Id, req.Descricao)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (r *registroHandlerGrpc) ListarTurno(ctx context.Context, req *registro.ListarTurnoRequest, rsp registro.Registro_ListarTurnoStream) error {
	log.Log("Received Registro.ListarTurno remote call")

	regs, err := r.ucase.ListarTurno(req.TurnoId, req.Page, req.Size)
	if err != nil {
		return err
	}

	for _, reg := range regs {
		msg := &registro.RegistroMessage{
			Id:           reg.Id,
			CaixaTurnoId: reg.CaixaTurnoId,
			Valor:        reg.Valor,
			Descricao:    reg.Descricao,
			Emissao:      core.ConvertToTimestamp(reg.Emissao),
		}
		_ = rsp.Send(msg)
	}
	return rsp.Close()
}
