package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/caixa-srv/application"
	caixa "bitbucket.org/murilocosta/halifax/caixa-srv/proto/caixa"

	"github.com/micro/go-micro/util/log"
)

type caixaHandlerGrpc struct {
	ucase *application.CaixaUseCase
}

func NewCaixaHandlerGrpc(ucase *application.CaixaUseCase) caixa.CaixaHandler {
	return &caixaHandlerGrpc{
		ucase: ucase,
	}
}

func (h *caixaHandlerGrpc) Criar(ctx context.Context, req *caixa.CriarCaixaRequest, rsp *caixa.CriarCaixaResponse) error {
	log.Log("Received Caixa.Criar remote call")

	uid, err := h.ucase.Criar(req.Nome)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (h *caixaHandlerGrpc) Atualizar(ctx context.Context, req *caixa.AlterarCaixaRequest, rsp *caixa.AlterarCaixaResponse) error {
	log.Log("Received Caixa.Atualizar remote call")

	uid, err := h.ucase.Atualizar(req.Id, req.Nome)
	if err != nil {
		return err
	}

	rsp.Id = uid
	return nil
}

func (h *caixaHandlerGrpc) Listar(ctx context.Context, req *caixa.ListarCaixasRequest, rsp caixa.Caixa_ListarStream) error {
	log.Log("Received Caixa.Listar remote call")

	cxs, err := h.ucase.Listar(req.Page, req.Size)
	if err != nil {
		return err
	}

	for _, cx := range cxs {
		_ = rsp.Send(&caixa.CaixaMessage{
			Id:   cx.Id,
			Nome: cx.Nome,
		})
	}
	return rsp.Close()
}
