package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/caixa-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro/util/log"
)

const (
	regInsertQuery             = "INSERT INTO registros (caixa_turno_id, valor, descricao, emissao) VALUES (?, ?, ?, ?);"
	regUpdateQuery             = "UPDATE registros SET descricao = ? WHERE id = ?"
	regFindByIdQuery           = "SELECT id, caixa_turno_id, valor, descricao, emissao FROM registros WHERE id = ?;"
	regFindByCaixaTurnoIdQuery = "SELECT r.id, r.caixa_turno_id, r.valor, r.descricao, r.emissao FROM registros AS r INNER JOIN caixa_turnos AS ct ON ct.id = r.caixa_turno_id WHERE ct.id = ? LIMIT ? OFFSET ?;"
)

type registroRepositoryMysql struct {
	db *sql.DB
}

func NewRegistroRepositoryMysql(db *sql.DB) domain.RegistroRepostory {
	return &registroRepositoryMysql{
		db: db,
	}
}

func (repo *registroRepositoryMysql) Save(r *domain.Registro) error {
	tx, err := repo.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(regInsertQuery)
	if err != nil {
		return err
	}

	rs, err := stmt.Exec(r.CaixaTurnoId, r.Valor, r.Descricao, r.Emissao)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}

	r.Id, _ = rs.LastInsertId()
	return tx.Commit()
}

func (repo *registroRepositoryMysql) Update(r *domain.Registro) error {
	tx, err := repo.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(regUpdateQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(r.Descricao, r.Id)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (repo *registroRepositoryMysql) FindById(id int64) (*domain.Registro, error) {
	row := repo.db.QueryRow(regFindByIdQuery, id)

	var reg domain.Registro
	err := row.Scan(&reg.Id, &reg.CaixaTurnoId, &reg.Valor, &reg.Descricao, &reg.Emissao)
	if err != nil {
		return nil, err
	}

	return &reg, nil
}

func (repo *registroRepositoryMysql) FindByCaixaTurnoId(caixaTurnoId int64, pg *core.Pagination) ([]*domain.Registro, error) {
	rows, err := repo.db.Query(regFindByCaixaTurnoIdQuery, caixaTurnoId, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	var regs []*domain.Registro
	for rows.Next() {
		var reg domain.Registro

		err := rows.Scan(&reg.Id, &reg.CaixaTurnoId, &reg.Valor, &reg.Descricao, &reg.Emissao)
		if err != nil {
			return nil, err
		}

		regs = append(regs, &reg)
	}

	return regs, nil
}
