package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/caixa-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/go-sql-driver/mysql"
	"github.com/micro/go-micro/util/log"
)

const (
	turnoInsertQuery                           = "INSERT INTO caixa_turnos(caixa_id, abertura, fundo_caixa) VALUES (?, ?, ?)"
	turnoUpdateQuery                           = "UPDATE caixa_turnos SET fechamento = ?, saldo_final = ? WHERE id = ?"
	turnoGetSaldoFinalQuery                    = "SELECT SUM(r.valor) FROM caixa_turnos AS ct INNER JOIN registros AS r on r.caixa_turno_id = ct.id WHERE ct.id = ?;"
	turnoFindByIdQuery                         = "SELECT id, caixa_id, abertura, fechamento, fundo_caixa, saldo_final FROM caixa_turnos WHERE id = ?;"
	turnoFindByCaixaIdAndFechamentoIsNullQuery = "SELECT id, caixa_id, abertura, fechamento, fundo_caixa, saldo_final FROM caixa_turnos WHERE caixa_id = ? AND fechamento IS NULL;"
	turnoFindAllByCaixaIdQuery                 = "SELECT id, caixa_id, abertura, fechamento, fundo_caixa, saldo_final FROM caixa_turnos WHERE caixa_id = ? LIMIT ? OFFSET ?;"
)

type caixaTurnoNull struct {
	fechamento mysql.NullTime
	fundoCaixa sql.NullFloat64
	saldoFinal sql.NullFloat64
}

func (n *caixaTurnoNull) Scan(t *domain.CaixaTurno) {
	if n.fechamento.Valid {
		t.Fechamento = n.fechamento.Time
	}
	if n.fundoCaixa.Valid {
		t.FundoCaixa = n.fundoCaixa.Float64
	}
	if n.saldoFinal.Valid {
		t.SaldoFinal = n.saldoFinal.Float64
	}
}

type turnoRepositoryMysql struct {
	db *sql.DB
}

func NewTurnoRepositoryMysql(db *sql.DB) domain.CaixaTurnoRepository {
	return &turnoRepositoryMysql{
		db: db,
	}
}

func (r *turnoRepositoryMysql) Save(t *domain.CaixaTurno) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(turnoInsertQuery)
	if err != nil {
		return err
	}

	rs, err := stmt.Exec(t.CaixaId, t.Abertura, t.FundoCaixa)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}

	t.Id, _ = rs.LastInsertId()
	return tx.Commit()
}

func (r *turnoRepositoryMysql) Update(t *domain.CaixaTurno) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(turnoUpdateQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(t.Fechamento, t.SaldoFinal, t.Id)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *turnoRepositoryMysql) GetSaldoFinal(id int64) (float64, error) {
	row := r.db.QueryRow(turnoGetSaldoFinalQuery, id)

	var saldoFinal sql.NullFloat64
	err := row.Scan(&saldoFinal)
	if err != nil {
		return 0, err
	}

	return saldoFinal.Float64, nil
}

func (r *turnoRepositoryMysql) FindById(id int64) (*domain.CaixaTurno, error) {
	row := r.db.QueryRow(turnoFindByIdQuery, id)
	return scanRow(row.Scan)
}

	turno, err := caixaTurnoRowMapper(row)
	if err == sql.ErrNoRows {
		return nil, nil
	}

func (r *turnoRepositoryMysql) FindAllByCaixaId(caixaId int64, pg *core.Pagination) ([]*domain.CaixaTurno, error) {
	rows, err := r.db.Query(turnoFindAllByCaixaIdQuery, caixaId, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	return turno, nil
}

func scanRow(scanFunc func(dest ...interface{}) error) (*domain.CaixaTurno, error) {
	var t domain.CaixaTurno
	tN := new(caixaTurnoNullable)

	turno, err := caixaTurnoRowMapper(row)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return turno, nil
}

	err = tN.Scan(&t)
	if err != nil {
		return nil, err
	}

	var turnos []*domain.CaixaTurno
	for rows.Next() {
		var t domain.CaixaTurno
		var n caixaTurnoNull
		rows.Scan(&t.Id, &t.CaixaId, &t.Abertura, &n.fechamento, &n.fundoCaixa, &n.saldoFinal)
		n.Scan(&t)
		turnos = append(turnos, &t)
	}

	return turnos, nil
}

func caixaTurnoRowMapper(row *sql.Row) (*domain.CaixaTurno, error) {
	var turno domain.CaixaTurno
	var turnoNull caixaTurnoNull

	err := row.Scan(&turno.Id, &turno.CaixaId, &turno.Abertura, &turnoNull.fechamento, &turnoNull.fundoCaixa, &turnoNull.saldoFinal)
	if err != nil {
		return nil, err
	}

	turnoNull.Scan(&turno)
	return &turno, nil
}
