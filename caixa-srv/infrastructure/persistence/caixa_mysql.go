package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/caixa-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro/util/log"
)

const (
	caixaInsertQuery   = "INSERT INTO caixas (nome) VALUES (?);"
	caixaUpdateQuery   = "UPDATE caixas SET nome = ? WHERE id = ?;"
	caixaFindByIdQuery = "SELECT id, nome from caixas WHERE id = ?;"
	caixaFindAllQuery  = "SELECT id, nome from caixas LIMIT ? OFFSET ?;"
)

type caixaRepositoryMysql struct {
	db *sql.DB
}

func NewCaixaRepositoryMysql(db *sql.DB) domain.CaixaRepository {
	return &caixaRepositoryMysql{
		db: db,
	}
}

func (r *caixaRepositoryMysql) Save(cx *domain.Caixa) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(caixaInsertQuery)
	if err != nil {
		return err
	}

	rs, err := stmt.Exec(cx.Nome)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}

	cx.Id, _ = rs.LastInsertId()
	return tx.Commit()
}

func (r *caixaRepositoryMysql) Update(cx *domain.Caixa) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(caixaUpdateQuery)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(cx.Nome, cx.Id)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (r *caixaRepositoryMysql) FindById(id int64) (*domain.Caixa, error) {
	row := r.db.QueryRow(caixaFindByIdQuery, id)

	var cx domain.Caixa
	err := row.Scan(&cx.Id, &cx.Nome)
	if err != nil {
		return nil, err
	}

	return &cx, nil
}

func (r *caixaRepositoryMysql) FindAll(pg *core.Pagination) ([]*domain.Caixa, error) {
	rows, err := r.db.Query(caixaFindAllQuery, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	var cxs []*domain.Caixa
	for rows.Next() {
		var cx domain.Caixa
		rows.Scan(&cx.Id, &cx.Nome)
		cxs = append(cxs, &cx)
	}

	return cxs, nil
}
