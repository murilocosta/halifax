package main

import (
	"database/sql"
	"os"

	"bitbucket.org/murilocosta/halifax/caixa-srv/application"
	"bitbucket.org/murilocosta/halifax/caixa-srv/infrastructure/persistence"
	"bitbucket.org/murilocosta/halifax/caixa-srv/infrastructure/transport"
	caixa "bitbucket.org/murilocosta/halifax/caixa-srv/proto/caixa"
	registro "bitbucket.org/murilocosta/halifax/caixa-srv/proto/registro"
	turno "bitbucket.org/murilocosta/halifax/caixa-srv/proto/turno"
	"bitbucket.org/murilocosta/halifax/caixa-srv/subscriber"
	"bitbucket.org/murilocosta/halifax/core"

	_ "github.com/go-sql-driver/mysql"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"github.com/micro/go-micro/util/log"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	dsl, _ := core.NewDataSourceLocation(
		config.DatabaseHost,
		config.DatabaseUser,
		config.DatabasePass,
		"halifax_caixa",
	)
	db, err := sql.Open(config.DatabaseDriver, dsl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.caixa"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	s := service.Server()
	registerCaixaHandler(db, s)
	registerRegistroHandler(db, s)
	registerTurnoHandler(db, s)

	// Register Struct as Subscriber
	micro.RegisterSubscriber("go.micro.srv.caixa", service.Server(), new(subscriber.Caixa))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func registerCaixaHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewCaixaRepositoryMysql(db)
	ucase := application.NewCaixaUseCase(repo)
	handler := transport.NewCaixaHandlerGrpc(ucase)
	return caixa.RegisterCaixaHandler(s, handler)
}

func registerRegistroHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewRegistroRepositoryMysql(db)
	ucase := application.NewRegistroUseCase(repo)
	handler := transport.NewRegistroHandlerGrpc(ucase)
	return registro.RegisterRegistroHandler(s, handler)
}

func registerTurnoHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewTurnoRepositoryMysql(db)
	ucase := application.NewTurnoUseCase(repo)
	handler := transport.NewTurnoHandlerGrpc(ucase)
	return turno.RegisterTurnoHandler(s, handler)
}
