package domain

import (
	"time"

	"bitbucket.org/murilocosta/halifax/core"
)

type CaixaTurno struct {
	Id         int64
	CaixaId    int64
	Abertura   time.Time
	Fechamento time.Time
	FundoCaixa float64
	SaldoFinal float64
}

func NewCaixaTurno(caixaId int64, fundoCaixa float64) *CaixaTurno {
	return &CaixaTurno{
		CaixaId:    caixaId,
		Abertura:   time.Now(),
		FundoCaixa: fundoCaixa,
	}
}

func (c *CaixaTurno) Fechar(saldoFinal float64) {
	c.SaldoFinal = c.FundoCaixa + saldoFinal
	c.Fechamento = time.Now()
}

type CaixaTurnoRepository interface {
	Save(t *CaixaTurno) error
	Update(t *CaixaTurno) error
	GetSaldoFinal(id int64) (float64, error)
	FindById(id int64) (*CaixaTurno, error)
	FindByCaixaIdAndFechamentoIsNull(caixaId int64) (*CaixaTurno, error)
	FindAllByCaixaId(caixaId int64, pg *core.Pagination) ([]*CaixaTurno, error)
}
