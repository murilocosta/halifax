package domain

import (
	"time"

	"bitbucket.org/murilocosta/halifax/core"
)

type Registro struct {
	Id           int64
	CaixaTurnoId int64
	Valor        float64
	Descricao    string
	Emissao      time.Time
}

func NewRegistro(caixaTurnoId int64, valor float64, descricao string, emissao time.Time) *Registro {
	return &Registro{
		CaixaTurnoId: caixaTurnoId,
		Valor:        valor,
		Descricao:    descricao,
		Emissao:      emissao,
	}
}

func (reg *Registro) Update(descricao string) {
	reg.Descricao = descricao
}

type RegistroRepostory interface {
	Save(r *Registro) error
	Update(r *Registro) error
	FindById(id int64) (*Registro, error)
	FindByCaixaTurnoId(caixaTurnoId int64, pg *core.Pagination) ([]*Registro, error)
}
