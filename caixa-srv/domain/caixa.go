package domain

import "bitbucket.org/murilocosta/halifax/core"

type Caixa struct {
	Id   int64
	Nome string
}

func NewCaixa(nome string) *Caixa {
	return &Caixa{
		Nome: nome,
	}
}

func (cx *Caixa) Update(nome string) {
	cx.Nome = nome
}

type CaixaRepository interface {
	Save(c *Caixa) error
	Update(c *Caixa) error
	FindById(id int64) (*Caixa, error)
	FindAll(pg *core.Pagination) ([]*Caixa, error)
}
