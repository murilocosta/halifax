package application

import (
	"time"

	"bitbucket.org/murilocosta/halifax/caixa-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"
)

type RegistroUseCase struct {
	repo domain.RegistroRepostory
}

func NewRegistroUseCase(repo domain.RegistroRepostory) *RegistroUseCase {
	return &RegistroUseCase{
		repo: repo,
	}
}

func (u *RegistroUseCase) Criar(caixaTurnoId int64, valor float64, descricao string, emissao time.Time) (int64, error) {
	reg := domain.NewRegistro(caixaTurnoId, valor, descricao, emissao)

	if err := u.repo.Save(reg); err != nil {
		return -1, err
	}

	return reg.Id, nil
}

func (u *RegistroUseCase) Atualizar(registroId int64, descricao string) (int64, error) {
	reg, err := u.repo.FindById(registroId)
	if err != nil {
		return -1, err
	}

	reg.Update(descricao)
	if err = u.repo.Update(reg); err != nil {
		return -1, err
	}

	return reg.Id, nil
}

func (u *RegistroUseCase) ListarTurno(caixaTurnoId int64, page int32, size int32) ([]*domain.Registro, error) {
	pg := core.NewPagination(page, size)
	return u.repo.FindByCaixaTurnoId(caixaTurnoId, pg)
}
