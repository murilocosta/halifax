package application

import (
	"errors"

	"bitbucket.org/murilocosta/halifax/caixa-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"
)

type TurnoUseCase struct {
	repo domain.CaixaTurnoRepository
}

func NewTurnoUseCase(repo domain.CaixaTurnoRepository) *TurnoUseCase {
	return &TurnoUseCase{
		repo: repo,
	}
}

func (ucase *TurnoUseCase) Iniciar(caixaId int64, fundoCaixa float64) (int64, error) {
	// Verifica se já existe um turno em aberto para esse caixa
	o, err := ucase.repo.FindByCaixaIdAndFechamentoIsNull(caixaId)
	if err != nil {
		return -1, err
	}

	if o != nil {
		return -1, errors.New("Já existe um turno aberto para esse caixa")
	}

	t := domain.NewCaixaTurno(caixaId, fundoCaixa)
	if err = ucase.repo.Save(t); err != nil {
		return -1, err
	}

	return t.Id, nil
}

func (ucase *TurnoUseCase) Finalizar(turnoId int64) (*domain.CaixaTurno, error) {
	t, err := ucase.repo.FindById(turnoId)
	if err != nil {
		return nil, err
	}

	if t == nil {
		return nil, errors.New("O turno informado não existe")
	}

	if !t.Fechamento.IsZero() {
		return nil, errors.New("O turno já está finalizado")
	}

	saldoFinal, err := ucase.repo.GetSaldoFinal(turnoId)
	if err != nil {
		return nil, err
	}

	t.Fechar(saldoFinal)

	if err = ucase.repo.Update(t); err != nil {
		return nil, err
	}

	return t, nil
}

func (ucase *TurnoUseCase) ListarPorCaixa(caixaId int64, page int32, size int32) ([]*domain.CaixaTurno, error) {
	pg := core.NewPagination(page, size)
	return ucase.repo.FindAllByCaixaId(caixaId, pg)
}
