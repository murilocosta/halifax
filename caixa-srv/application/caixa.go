package application

import (
	"bitbucket.org/murilocosta/halifax/caixa-srv/domain"
	"bitbucket.org/murilocosta/halifax/core"
)

type CaixaUseCase struct {
	repo domain.CaixaRepository
}

func NewCaixaUseCase(repo domain.CaixaRepository) *CaixaUseCase {
	return &CaixaUseCase{
		repo: repo,
	}
}

func (ucase *CaixaUseCase) Criar(nome string) (int64, error) {
	cx := domain.NewCaixa(nome)

	if err := ucase.repo.Save(cx); err != nil {
		return -1, err
	}

	return cx.Id, nil
}

func (ucase *CaixaUseCase) Atualizar(id int64, nome string) (int64, error) {
	cx, err := ucase.repo.FindById(id)
	if err != nil {
		return -1, err
	}

	cx.Update(nome)
	if err = ucase.repo.Update(cx); err != nil {
		return -1, err
	}

	return cx.Id, nil
}

func (ucase *CaixaUseCase) Listar(page int32, size int32) ([]*domain.Caixa, error) {
	pg := core.NewPagination(page, size)
	return ucase.repo.FindAll(pg)
}
