#!/bin/bash

export CGO_ENABLED=0

cd caixa-srv
make build

cd ../caixa-api
make build

cd ../estoque-srv
make build

cd ../estoque-api
make build

cd ../financeiro-srv
make build

cd ../financeiro-api
make build

cd ../compras-srv
make build

cd ../compras-api
make build

cd ../vendas-srv
make build

cd ../vendas-api
make build
