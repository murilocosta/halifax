package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/vendas-api/client"
	vendasApi "bitbucket.org/murilocosta/halifax/vendas-api/proto/vendas"
	vendas "bitbucket.org/murilocosta/halifax/vendas-srv/proto/vendas"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type vendaHandlerProxy struct{}

func NewVendaHandlerProxy() vendasApi.VendaHandler {
	return &vendaHandlerProxy{}
}

func (*vendaHandlerProxy) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Vendas.Criar request")

	vendasClient, ok := client.VendaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.vendas", "vendas client not found")
	}

	var payload vendas.CriarRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.vendas", err.Error())
	}

	response, err := vendasClient.Criar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.vendas", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*vendaHandlerProxy) Alterar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Vendas.Alterar request")

	vendasClient, ok := client.VendaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.vendas", "vendas client not found")
	}

	var payload vendas.AlterarRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.vendas", err.Error())
	}

	response, err := vendasClient.Alterar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.vendas", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*vendaHandlerProxy) Buscar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Vendas.Buscar request")

	vendasClient, ok := client.VendaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.vendas", "vendas client not found")
	}

	id := core.ExtractValueFromRequestAsInt64(req.Get["id"], 0)
	response, err := vendasClient.Buscar(ctx, &vendas.BuscarRequest{Id: id})
	if err != nil {
		return errors.BadRequest("go.micro.api.vendas", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*vendaHandlerProxy) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Vendas.Buscar request")

	vendasClient, ok := client.VendaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.vendas", "vendas client not found")
	}

	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := vendasClient.Listar(ctx, &vendas.ListarRequest{
		Page: page * size,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.vendas", err.Error())
	}

	var items []*vendas.VendaMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.vendas", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
