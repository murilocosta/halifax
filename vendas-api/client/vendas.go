package client

import (
	"context"

	vendas "bitbucket.org/murilocosta/halifax/vendas-srv/proto/vendas"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type vendasKey struct{}

// FromContext retrieves the client from the Context
func VendaFromContext(ctx context.Context) (vendas.VendaService, bool) {
	c, ok := ctx.Value(vendasKey{}).(vendas.VendaService)
	return c, ok
}

// Client returns a wrapper for the VendasClient
func VendaWrapper(service micro.Service) server.HandlerWrapper {
	client := vendas.NewVendaService("go.micro.srv.vendas", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, vendasKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
