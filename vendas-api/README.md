# Vendas Service

This is the Vendas service

Generated with

```
micro new vendas-api --namespace=bitbucket.org/murilocosta/halifax --type=api
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: bitbucket.org/murilocosta/halifax.api.vendas
- Type: api
- Alias: vendas

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./vendas-api
```

Build a docker image
```
make docker
```