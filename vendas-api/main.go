package main

import (
	"os"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/vendas-api/client"
	"bitbucket.org/murilocosta/halifax/vendas-api/handler"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"

	vendas "bitbucket.org/murilocosta/halifax/vendas-api/proto/vendas"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.api.vendas"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init(
		// create wrap for the Vendas srv client
		micro.WrapHandler(client.VendaWrapper(service)),
	)

	// Register Handler
	vendas.RegisterVendaHandler(service.Server(), handler.NewVendaHandlerProxy())

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
