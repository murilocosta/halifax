package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/caixa-api/client"
	caixaApi "bitbucket.org/murilocosta/halifax/caixa-api/proto/caixa"
	caixa "bitbucket.org/murilocosta/halifax/caixa-srv/proto/caixa"
	"bitbucket.org/murilocosta/halifax/core"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type caixaHandlerProxy struct{}

func NewCaixaHandlerProxy() caixaApi.CaixaHandler {
	return &caixaHandlerProxy{}
}

func (*caixaHandlerProxy) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Caixa.Criar request")

	caixaClient, ok := client.CaixaFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "caixa client not found")
	}

	// make request
	var payload caixa.CriarCaixaRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	response, err := caixaClient.Criar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*caixaHandlerProxy) Atualizar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Caixa.Atualizar request")

	caixaClient, ok := client.CaixaFromContext(ctx)
	if ! ok {
		return errors.InternalServerError("go.micro.api.caixa", "caixa client not found")
	}

	var payload caixa.AlterarCaixaRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	response, err := caixaClient.Atualizar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*caixaHandlerProxy) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Caixa.Listar request")

	caixaClient, ok := client.CaixaFromContext(ctx)
	if ! ok {
		return errors.InternalServerError("go.micro.api.caixa", "caixa client not found")
	}

	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := caixaClient.Listar(ctx, &caixa.ListarCaixasRequest{
		Page: page,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	var items []*caixa.CaixaMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
