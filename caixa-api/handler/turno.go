package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/caixa-api/client"
	turnoApi "bitbucket.org/murilocosta/halifax/caixa-api/proto/caixa"
	turno "bitbucket.org/murilocosta/halifax/caixa-srv/proto/turno"
	"bitbucket.org/murilocosta/halifax/core"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type turnoHandlerProxy struct{}

func NewTurnoHandlerProxy() turnoApi.CaixaTurnoHandler {
	return &turnoHandlerProxy{}
}

func (*turnoHandlerProxy) Iniciar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received CaixaTurno.Iniciar request")

	turnoClient, ok := client.CaixaTurnoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "caixa turno client not found")
	}

	var payload turno.IniciarTurnoRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	response, err := turnoClient.Iniciar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*turnoHandlerProxy) Finalizar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received CaixaTurno.Finalizar request")

	turnoClient, ok := client.CaixaTurnoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "caixa turno client not found")
	}

	var payload turno.FinalizarTurnoRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	response, err := turnoClient.Finalizar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*turnoHandlerProxy) ListarPorCaixa(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received CaixaTurno.ListarPorCaixa request")

	turnoClient, ok := client.CaixaTurnoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "caixa turno client not found")
	}

	caixaId := core.ExtractValueFromRequestAsInt64(req.Get["caixaId"], 0)
	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	response, err := turnoClient.ListarPorCaixa(ctx, &turno.ListarPorCaixaRequest{
		CaixaId: caixaId,
		Page:    page,
		Size:    size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	var items []*turno.TurnoMessage
	for {
		item, err := response.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
