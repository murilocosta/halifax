package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/caixa-api/client"
	registroApi "bitbucket.org/murilocosta/halifax/caixa-api/proto/caixa"
	registro "bitbucket.org/murilocosta/halifax/caixa-srv/proto/registro"
	"bitbucket.org/murilocosta/halifax/core"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type registroHandlerProxy struct{}

func NewRegistroHandlerProxy() registroApi.RegistroHandler {
	return &registroHandlerProxy{}
}

func (*registroHandlerProxy) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Registro.Criar request")

	registroClient, ok := client.RegistroFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "registro client not found")
	}

	var payload registro.RegistroMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	response, err := registroClient.Criar(ctx, &registro.CriarRegistroRequest{
		Registro: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*registroHandlerProxy) Atualizar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Registro.Atualizar request")

	registroClient, ok := client.RegistroFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "registro client not found")
	}

	var payload registro.AtualizarRegistroRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	response, err := registroClient.Atualizar(ctx, &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*registroHandlerProxy) ListarTurno(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Registro.ListarTurno request")

	registroClient, ok := client.RegistroFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.caixa", "registro client not found")
	}

	turnoId := core.ExtractValueFromRequestAsInt64(req.Get["turnoId"], 0)
	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := registroClient.ListarTurno(ctx, &registro.ListarTurnoRequest{
		TurnoId: turnoId,
		Page:    page,
		Size:    size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.caixa", err.Error())
	}

	var items []*registro.RegistroMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.caixa", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
