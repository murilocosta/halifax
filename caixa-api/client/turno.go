package client

import (
	"context"

	turno "bitbucket.org/murilocosta/halifax/caixa-srv/proto/turno"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type turnoKey struct{}

func CaixaTurnoFromContext(ctx context.Context) (turno.TurnoService, bool) {
	t, ok := ctx.Value(turnoKey{}).(turno.TurnoService)
	return t, ok
}

func CaixaTurnoWrapper(service micro.Service) server.HandlerWrapper {
	client := turno.NewTurnoService("go.micro.srv.caixa", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, turnoKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
