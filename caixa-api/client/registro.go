package client

import (
	"context"

	registro "bitbucket.org/murilocosta/halifax/caixa-srv/proto/registro"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type registroKey struct{}

func RegistroFromContext(ctx context.Context) (registro.RegistroService, bool) {
	r, ok := ctx.Value(registroKey{}).(registro.RegistroService)
	return r, ok
}

func RegistroWrapper(service micro.Service) server.HandlerWrapper {
	client := registro.NewRegistroService("go.micro.srv.caixa", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, registroKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
