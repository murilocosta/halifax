package client

import (
	"context"

	caixa "bitbucket.org/murilocosta/halifax/caixa-srv/proto/caixa"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type caixaKey struct{}

func CaixaFromContext(ctx context.Context) (caixa.CaixaService, bool) {
	c, ok := ctx.Value(caixaKey{}).(caixa.CaixaService)
	return c, ok
}

func CaixaWrapper(service micro.Service) server.HandlerWrapper {
	client := caixa.NewCaixaService("go.micro.srv.caixa", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, caixaKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
