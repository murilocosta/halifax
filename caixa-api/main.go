package main

import (
	"os"

	"bitbucket.org/murilocosta/halifax/caixa-api/client"
	"bitbucket.org/murilocosta/halifax/caixa-api/handler"
	caixa "bitbucket.org/murilocosta/halifax/caixa-api/proto/caixa"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.api.caixa"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init(
		// create wrap for the Caixa srv client
		micro.WrapHandler(client.CaixaWrapper(service)),
		micro.WrapHandler(client.CaixaTurnoWrapper(service)),
		micro.WrapHandler(client.RegistroWrapper(service)),
	)

	// Register Handler
	caixa.RegisterCaixaHandler(service.Server(), handler.NewCaixaHandlerProxy())
	caixa.RegisterCaixaTurnoHandler(service.Server(), handler.NewTurnoHandlerProxy())
	caixa.RegisterRegistroHandler(service.Server(), handler.NewRegistroHandlerProxy())

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
