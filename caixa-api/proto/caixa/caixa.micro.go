// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/caixa/caixa.proto

package go_micro_api_caixa

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	proto1 "github.com/micro/go-micro/api/proto"
	math "math"
)

import (
	context "context"
	client "github.com/micro/go-micro/client"
	server "github.com/micro/go-micro/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ client.Option
var _ server.Option

// Client API for Caixa service

type CaixaService interface {
	Criar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
	Atualizar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
	Listar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
}

type caixaService struct {
	c    client.Client
	name string
}

func NewCaixaService(name string, c client.Client) CaixaService {
	if c == nil {
		c = client.NewClient()
	}
	if len(name) == 0 {
		name = "go.micro.api.caixa"
	}
	return &caixaService{
		c:    c,
		name: name,
	}
}

func (c *caixaService) Criar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "Caixa.Criar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *caixaService) Atualizar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "Caixa.Atualizar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *caixaService) Listar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "Caixa.Listar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Caixa service

type CaixaHandler interface {
	Criar(context.Context, *proto1.Request, *proto1.Response) error
	Atualizar(context.Context, *proto1.Request, *proto1.Response) error
	Listar(context.Context, *proto1.Request, *proto1.Response) error
}

func RegisterCaixaHandler(s server.Server, hdlr CaixaHandler, opts ...server.HandlerOption) error {
	type caixa interface {
		Criar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
		Atualizar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
		Listar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
	}
	type Caixa struct {
		caixa
	}
	h := &caixaHandler{hdlr}
	return s.Handle(s.NewHandler(&Caixa{h}, opts...))
}

type caixaHandler struct {
	CaixaHandler
}

func (h *caixaHandler) Criar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.CaixaHandler.Criar(ctx, in, out)
}

func (h *caixaHandler) Atualizar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.CaixaHandler.Atualizar(ctx, in, out)
}

func (h *caixaHandler) Listar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.CaixaHandler.Listar(ctx, in, out)
}

// Client API for Registro service

type RegistroService interface {
	Criar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
	Atualizar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
	ListarTurno(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
}

type registroService struct {
	c    client.Client
	name string
}

func NewRegistroService(name string, c client.Client) RegistroService {
	if c == nil {
		c = client.NewClient()
	}
	if len(name) == 0 {
		name = "go.micro.api.caixa"
	}
	return &registroService{
		c:    c,
		name: name,
	}
}

func (c *registroService) Criar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "Registro.Criar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *registroService) Atualizar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "Registro.Atualizar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *registroService) ListarTurno(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "Registro.ListarTurno", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Registro service

type RegistroHandler interface {
	Criar(context.Context, *proto1.Request, *proto1.Response) error
	Atualizar(context.Context, *proto1.Request, *proto1.Response) error
	ListarTurno(context.Context, *proto1.Request, *proto1.Response) error
}

func RegisterRegistroHandler(s server.Server, hdlr RegistroHandler, opts ...server.HandlerOption) error {
	type registro interface {
		Criar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
		Atualizar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
		ListarTurno(ctx context.Context, in *proto1.Request, out *proto1.Response) error
	}
	type Registro struct {
		registro
	}
	h := &registroHandler{hdlr}
	return s.Handle(s.NewHandler(&Registro{h}, opts...))
}

type registroHandler struct {
	RegistroHandler
}

func (h *registroHandler) Criar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.RegistroHandler.Criar(ctx, in, out)
}

func (h *registroHandler) Atualizar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.RegistroHandler.Atualizar(ctx, in, out)
}

func (h *registroHandler) ListarTurno(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.RegistroHandler.ListarTurno(ctx, in, out)
}

// Client API for CaixaTurno service

type CaixaTurnoService interface {
	Iniciar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
	Finalizar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
	ListarPorCaixa(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error)
}

type caixaTurnoService struct {
	c    client.Client
	name string
}

func NewCaixaTurnoService(name string, c client.Client) CaixaTurnoService {
	if c == nil {
		c = client.NewClient()
	}
	if len(name) == 0 {
		name = "go.micro.api.caixa"
	}
	return &caixaTurnoService{
		c:    c,
		name: name,
	}
}

func (c *caixaTurnoService) Iniciar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "CaixaTurno.Iniciar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *caixaTurnoService) Finalizar(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "CaixaTurno.Finalizar", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *caixaTurnoService) ListarPorCaixa(ctx context.Context, in *proto1.Request, opts ...client.CallOption) (*proto1.Response, error) {
	req := c.c.NewRequest(c.name, "CaixaTurno.ListarPorCaixa", in)
	out := new(proto1.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for CaixaTurno service

type CaixaTurnoHandler interface {
	Iniciar(context.Context, *proto1.Request, *proto1.Response) error
	Finalizar(context.Context, *proto1.Request, *proto1.Response) error
	ListarPorCaixa(context.Context, *proto1.Request, *proto1.Response) error
}

func RegisterCaixaTurnoHandler(s server.Server, hdlr CaixaTurnoHandler, opts ...server.HandlerOption) error {
	type caixaTurno interface {
		Iniciar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
		Finalizar(ctx context.Context, in *proto1.Request, out *proto1.Response) error
		ListarPorCaixa(ctx context.Context, in *proto1.Request, out *proto1.Response) error
	}
	type CaixaTurno struct {
		caixaTurno
	}
	h := &caixaTurnoHandler{hdlr}
	return s.Handle(s.NewHandler(&CaixaTurno{h}, opts...))
}

type caixaTurnoHandler struct {
	CaixaTurnoHandler
}

func (h *caixaTurnoHandler) Iniciar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.CaixaTurnoHandler.Iniciar(ctx, in, out)
}

func (h *caixaTurnoHandler) Finalizar(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.CaixaTurnoHandler.Finalizar(ctx, in, out)
}

func (h *caixaTurnoHandler) ListarPorCaixa(ctx context.Context, in *proto1.Request, out *proto1.Response) error {
	return h.CaixaTurnoHandler.ListarPorCaixa(ctx, in, out)
}
