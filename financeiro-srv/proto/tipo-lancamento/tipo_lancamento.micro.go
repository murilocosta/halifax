// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/tipo-lancamento/tipo_lancamento.proto

package go_micro_srv_financeiro

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

import (
	context "context"
	client "github.com/micro/go-micro/client"
	server "github.com/micro/go-micro/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ client.Option
var _ server.Option

// Client API for TipoLancamento service

type TipoLancamentoService interface {
	Criar(ctx context.Context, in *CriarTipoLancamentoRequest, opts ...client.CallOption) (*CriarTipoLancamentoResponse, error)
	Alterar(ctx context.Context, in *AlterarTipoLancamentoRequest, opts ...client.CallOption) (*AlterarTipoLancamentoResponse, error)
	Listar(ctx context.Context, in *ListarTipoLancamentosRequest, opts ...client.CallOption) (TipoLancamento_ListarService, error)
}

type tipoLancamentoService struct {
	c    client.Client
	name string
}

func NewTipoLancamentoService(name string, c client.Client) TipoLancamentoService {
	if c == nil {
		c = client.NewClient()
	}
	if len(name) == 0 {
		name = "go.micro.srv.financeiro"
	}
	return &tipoLancamentoService{
		c:    c,
		name: name,
	}
}

func (c *tipoLancamentoService) Criar(ctx context.Context, in *CriarTipoLancamentoRequest, opts ...client.CallOption) (*CriarTipoLancamentoResponse, error) {
	req := c.c.NewRequest(c.name, "TipoLancamento.Criar", in)
	out := new(CriarTipoLancamentoResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tipoLancamentoService) Alterar(ctx context.Context, in *AlterarTipoLancamentoRequest, opts ...client.CallOption) (*AlterarTipoLancamentoResponse, error) {
	req := c.c.NewRequest(c.name, "TipoLancamento.Alterar", in)
	out := new(AlterarTipoLancamentoResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tipoLancamentoService) Listar(ctx context.Context, in *ListarTipoLancamentosRequest, opts ...client.CallOption) (TipoLancamento_ListarService, error) {
	req := c.c.NewRequest(c.name, "TipoLancamento.Listar", &ListarTipoLancamentosRequest{})
	stream, err := c.c.Stream(ctx, req, opts...)
	if err != nil {
		return nil, err
	}
	if err := stream.Send(in); err != nil {
		return nil, err
	}
	return &tipoLancamentoServiceListar{stream}, nil
}

type TipoLancamento_ListarService interface {
	SendMsg(interface{}) error
	RecvMsg(interface{}) error
	Close() error
	Recv() (*TipoLancamentoMessage, error)
}

type tipoLancamentoServiceListar struct {
	stream client.Stream
}

func (x *tipoLancamentoServiceListar) Close() error {
	return x.stream.Close()
}

func (x *tipoLancamentoServiceListar) SendMsg(m interface{}) error {
	return x.stream.Send(m)
}

func (x *tipoLancamentoServiceListar) RecvMsg(m interface{}) error {
	return x.stream.Recv(m)
}

func (x *tipoLancamentoServiceListar) Recv() (*TipoLancamentoMessage, error) {
	m := new(TipoLancamentoMessage)
	err := x.stream.Recv(m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

// Server API for TipoLancamento service

type TipoLancamentoHandler interface {
	Criar(context.Context, *CriarTipoLancamentoRequest, *CriarTipoLancamentoResponse) error
	Alterar(context.Context, *AlterarTipoLancamentoRequest, *AlterarTipoLancamentoResponse) error
	Listar(context.Context, *ListarTipoLancamentosRequest, TipoLancamento_ListarStream) error
}

func RegisterTipoLancamentoHandler(s server.Server, hdlr TipoLancamentoHandler, opts ...server.HandlerOption) error {
	type tipoLancamento interface {
		Criar(ctx context.Context, in *CriarTipoLancamentoRequest, out *CriarTipoLancamentoResponse) error
		Alterar(ctx context.Context, in *AlterarTipoLancamentoRequest, out *AlterarTipoLancamentoResponse) error
		Listar(ctx context.Context, stream server.Stream) error
	}
	type TipoLancamento struct {
		tipoLancamento
	}
	h := &tipoLancamentoHandler{hdlr}
	return s.Handle(s.NewHandler(&TipoLancamento{h}, opts...))
}

type tipoLancamentoHandler struct {
	TipoLancamentoHandler
}

func (h *tipoLancamentoHandler) Criar(ctx context.Context, in *CriarTipoLancamentoRequest, out *CriarTipoLancamentoResponse) error {
	return h.TipoLancamentoHandler.Criar(ctx, in, out)
}

func (h *tipoLancamentoHandler) Alterar(ctx context.Context, in *AlterarTipoLancamentoRequest, out *AlterarTipoLancamentoResponse) error {
	return h.TipoLancamentoHandler.Alterar(ctx, in, out)
}

func (h *tipoLancamentoHandler) Listar(ctx context.Context, stream server.Stream) error {
	m := new(ListarTipoLancamentosRequest)
	if err := stream.Recv(m); err != nil {
		return err
	}
	return h.TipoLancamentoHandler.Listar(ctx, m, &tipoLancamentoListarStream{stream})
}

type TipoLancamento_ListarStream interface {
	SendMsg(interface{}) error
	RecvMsg(interface{}) error
	Close() error
	Send(*TipoLancamentoMessage) error
}

type tipoLancamentoListarStream struct {
	stream server.Stream
}

func (x *tipoLancamentoListarStream) Close() error {
	return x.stream.Close()
}

func (x *tipoLancamentoListarStream) SendMsg(m interface{}) error {
	return x.stream.Send(m)
}

func (x *tipoLancamentoListarStream) RecvMsg(m interface{}) error {
	return x.stream.Recv(m)
}

func (x *tipoLancamentoListarStream) Send(m *TipoLancamentoMessage) error {
	return x.stream.Send(m)
}
