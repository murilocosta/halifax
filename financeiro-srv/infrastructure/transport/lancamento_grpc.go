package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/application"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"

	"github.com/micro/go-micro/util/log"

	lancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/lancamento"
)

type lancamentoHandlerGrpc struct {
	ucase *application.LancamentoUseCase
}

func NewLancamentoHandlerGrpc(ucase *application.LancamentoUseCase) lancamento.LancamentoHandler {
	return &lancamentoHandlerGrpc{
		ucase: ucase,
	}
}

func (h *lancamentoHandlerGrpc) Registrar(ctx context.Context, req *lancamento.RegistrarLancamentoRequest, rsp *lancamento.RegistrarLancamentoResponse) error {
	log.Log("Received Lancamento.Registrar remote call")

	emissao := core.ConvertFromTimestamp(req.Lancamento.Emissao)

	lanc := domain.NewLancamento(
		req.Lancamento.RegistroId,
		req.Lancamento.TipoLancamentoId,
		req.Lancamento.ContaId,
		req.Lancamento.Valor,
		req.Lancamento.Descricao,
		emissao,
	)

	err := h.ucase.Registrar(lanc)
	if err != nil {
		return err
	}

	rsp.Id = lanc.Id
	return nil
}

func (h *lancamentoHandlerGrpc) Atualizar(ctx context.Context, req *lancamento.AtualizarLancamentoRequest, rsp *lancamento.AtualizarLancamentoResponse) error {
	log.Log("Received Lancamento.Atualizar remote call")

	emissao := core.ConvertFromTimestamp(req.Emissao)

	err := h.ucase.Atualizar(req.Id, req.TipoLancamentoId, req.Descricao, emissao)
	if err != nil {
		return err
	}

	rsp.Id = req.Id
	return nil
}

func (h *lancamentoHandlerGrpc) Estornar(ctx context.Context, req *lancamento.EstornarLancamentoRequest, rsp *lancamento.EstornarLancamentoResponse) error {
	log.Log("Received Lancamento.Estornar remote call")

	return nil
}

func (h *lancamentoHandlerGrpc) Agendar(ctx context.Context, req *lancamento.AgendarLancamentoRequest, rsp *lancamento.AgendarLancamentoResponse) error {
	log.Log("Received Lancamento.Agendar remote call")

	validade := core.ConvertFromTimestamp(req.Lancamento.Validade)
	emissao := core.ConvertFromTimestamp(req.Lancamento.Emissao)

	lancFut := domain.NewLancamentoFuturo(
		req.Lancamento.RegistroId,
		req.Lancamento.ContaDestinoId,
		req.Lancamento.TipoLancamentoDestinoId,
		req.Lancamento.Valor,
		req.Lancamento.Descricao,
		validade,
		emissao,
	)

	err := h.ucase.Agendar(lancFut)
	if err != nil {
		return err
	}
	return nil
}

func (h *lancamentoHandlerGrpc) ListarConta(ctx context.Context, req *lancamento.ListarLancamentosContaRequest, rsp lancamento.Lancamento_ListarContaStream) error {
	log.Log("Received Lancamento.ListarConta remote call")

	lancContas, err := h.ucase.ListarConta(req.ContaId, req.Page, req.Size)
	if err != nil {
		return err
	}

	for _, lancConta := range lancContas {
		rsp.Send(convertToLancamentoContaMessage(lancConta))
	}
	return rsp.Close()
}

func convertToLancamentoContaMessage(lancConta *domain.LancamentoConta) *lancamento.LancamentoContaMessage {
	return &lancamento.LancamentoContaMessage{
		ContaId:    lancConta.ContaId,
		Nome:       lancConta.ContaNome,
		Lancamento: convertToLancamentoMessage(lancConta.Lancamento),
	}
}

func convertToLancamentoMessage(lanc *domain.Lancamento) *lancamento.LancamentoMessage {
	return &lancamento.LancamentoMessage{
		Id:               lanc.Id,
		ContaId:          lanc.ContaId,
		TipoLancamentoId: lanc.TipoLancamentoId,
		Descricao:        lanc.Descricao,
		Emissao:          core.ConvertToTimestamp(lanc.Emissao),
		RegistroId:       lanc.RegistroId,
	}
}
