package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/financeiro-srv/application"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
	tipoLancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/tipo-lancamento"

	"github.com/micro/go-micro/util/log"
)

type tipoLancamentoHandlerGrpc struct {
	ucase *application.TipoLancamentoUseCase
}

func NewTipoLancamentoHandlerGrpc(ucase *application.TipoLancamentoUseCase) tipoLancamento.TipoLancamentoHandler {
	return &tipoLancamentoHandlerGrpc{
		ucase: ucase,
	}
}

func (h *tipoLancamentoHandlerGrpc) Criar(ctx context.Context, req *tipoLancamento.CriarTipoLancamentoRequest, rsp *tipoLancamento.CriarTipoLancamentoResponse) error {
	log.Log("Received TipoLancamento.Criar remote call")

	uid, err := h.ucase.Criar(req.Tipo.Nome)
	if err != nil {
		return err
	}
	rsp.Id = uid
	return nil
}

func (h *tipoLancamentoHandlerGrpc) Alterar(ctx context.Context, req *tipoLancamento.AlterarTipoLancamentoRequest, rsp *tipoLancamento.AlterarTipoLancamentoResponse) error {
	log.Log("Received TipoLancamento.Alterar remote call")

	err := h.ucase.Alterar(req.Tipo.Id, req.Tipo.Nome)
	if err != nil {
		return err
	}
	rsp.Id = req.Tipo.Id
	return nil
}

func (h *tipoLancamentoHandlerGrpc) Listar(ctx context.Context, req *tipoLancamento.ListarTipoLancamentosRequest, rsp tipoLancamento.TipoLancamento_ListarStream) error {
	log.Log("Received TipoLancamento.Listar remote call")

	tpLncs, err := h.ucase.Listar(req.Page, req.Size)
	if err != nil {
		return err
	}
	for _, tpLnc := range tpLncs {
		rsp.Send(convertToTipoLancamentoMessage(tpLnc))
	}
	return rsp.Close()
}

func convertToTipoLancamentoMessage(tpLnc *domain.TipoLancamento) *tipoLancamento.TipoLancamentoMessage {
	return &tipoLancamento.TipoLancamentoMessage{
		Id:   tpLnc.Id,
		Nome: tpLnc.Nome,
	}
}
