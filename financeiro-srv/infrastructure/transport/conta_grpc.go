package transport

import (
	"context"

	"bitbucket.org/murilocosta/halifax/financeiro-srv/application"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
	conta "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/conta"

	"github.com/micro/go-micro/util/log"
)

type contaHandlerGrpc struct {
	ucase *application.ContaUseCase
}

func NewContaHandlerGrpc(ucase *application.ContaUseCase) conta.ContaHandler {
	return &contaHandlerGrpc{
		ucase: ucase,
	}
}

func (h *contaHandlerGrpc) Criar(ctx context.Context, req *conta.CriarContaRequest, rsp *conta.CriarContaResponse) error {
	log.Log("Received Conta.Criar remote call")

	uid, err := h.ucase.Criar(req.Conta.Nome)
	if err != nil {
		return err
	}
	rsp.Id = uid
	return nil
}

func (h *contaHandlerGrpc) Alterar(ctx context.Context, req *conta.AlterarContaRequest, rsp *conta.AlterarContaResponse) error {
	log.Log("Received Conta.Alterar remote call")

	err := h.ucase.Alterar(req.Conta.Id, req.Conta.Nome)
	if err != nil {
		return err
	}
	rsp.Id = req.Conta.Id
	return nil
}

func (h *contaHandlerGrpc) Listar(ctx context.Context, req *conta.ListarContasRequest, rsp conta.Conta_ListarStream) error {
	log.Log("Received Conta.Listar remote call")

	cnts, err := h.ucase.Listar(req.Page, req.Size)
	if err != nil {
		return err
	}
	for _, cnt := range cnts {
		rsp.Send(convertToContaMessage(cnt))
	}
	return rsp.Close()
}

func convertToContaMessage(cnt *domain.Conta) *conta.ContaMessage {
	return &conta.ContaMessage{
		Id:   cnt.Id,
		Nome: cnt.Nome,
	}
}
