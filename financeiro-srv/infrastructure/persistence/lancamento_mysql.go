package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"

	"github.com/micro/go-micro/util/log"
)

const (
	lancInsertQuery        = "INSERT INTO lancamentos (conta_id, tipo_lancamento_id, valor, descricao, emissao, registroId) VALUES (?, ?, ?, ?, ?, ?);"
	lancUpdateQuery        = "UPDATE lancamentos SET tipo_lancamento_id = ?, descricao = ?, emissao = ? WHERE id = ?;"
	lancFindByIdQuery      = "SELECT id, conta_id, tipo_lancamento_id, valor, descricao, emissao, registroId FROM lancamentos WHERE id = ?;"
	lancFindByContaIdQuery = "SELECT l.id, l.conta_id, l.tipo_lancamento_id, l.valor, l.descricao, l.emissao, l.registroId, c.id, c.nome FROM lancamentos AS l INNER JOIN contas AS c on c.id = l.conta_id  WHERE c.conta_id = ? LIMIT ? OFFSET ?;"
	lancEstInsertQuery     = "INSERT INTO lancamentos_estornos (origem_id, destino_id, emissao) VALUES (?, ?, ?);"
)

type lancamentoRepositoryMysql struct {
	db      *sql.DB
	adapter *core.MysqlAdapter
}

func NewLancamentoRepositoryMysql(db *sql.DB) domain.LancamentoRepository {
	return &lancamentoRepositoryMysql{
		db:      db,
		adapter: core.NewMysqlAdapter(db),
	}
}

func (r *lancamentoRepositoryMysql) Save(l *domain.Lancamento) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(l.ContaId, l.TipoLancamentoId, l.Valor, l.Descricao, l.Emissao, l.RegistroId)
	}

	uid, err := r.adapter.SaveQuery(lancInsertQuery, executor)
	if err != nil {
		return err
	}
	l.Id = uid

	return nil
}

func (r *lancamentoRepositoryMysql) Update(l *domain.Lancamento) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(l.TipoLancamentoId, l.Descricao, l.Emissao, l.Id)
	}
	return r.adapter.UpdateQuery(lancUpdateQuery, executor)
}

func (r *lancamentoRepositoryMysql) Delete(lanc *domain.Lancamento) (*domain.Lancamento, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return nil, err
	}

	stmt, err := tx.Prepare(lancInsertQuery)
	if err != nil {
		return nil, err
	}

	delLanc := lanc.Estornar()
	rs, err := stmt.Exec(
		delLanc.ContaId,
		delLanc.TipoLancamentoId,
		delLanc.Valor,
		delLanc.Descricao,
		delLanc.Emissao,
		delLanc.RegistroId,
	)
	err = rollbackOnError(tx, err)
	if err != nil {
		return nil, err
	}
	delLanc.Id, _ = rs.LastInsertId()

	estorno := domain.NewLancamentoEstorno(lanc.Id, delLanc.Id)
	stmt, err = tx.Prepare(lancEstInsertQuery)
	_, err = stmt.Exec(estorno.OrigemId, estorno.DestinoId, estorno.Emissao)
	err = rollbackOnError(tx, err)
	if err != nil {
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}
	return delLanc, nil
}

func (r *lancamentoRepositoryMysql) FindById(id int64) (*domain.Lancamento, error) {
	var lanc domain.Lancamento

	mapper := func(row *sql.Row) error {
		return row.Scan(
			&lanc.Id,
			&lanc.ContaId,
			&lanc.TipoLancamentoId,
			&lanc.Valor,
			&lanc.Descricao,
			&lanc.Emissao,
			&lanc.RegistroId,
		)
	}

	err := r.adapter.FindByIdQuery(mapper, lancFindByIdQuery, id)
	if err != nil {
		return nil, err
	}

	return &lanc, nil
}

func (r *lancamentoRepositoryMysql) FindByContaId(contaId int64, pg *core.Pagination) ([]*domain.LancamentoConta, error) {
	var lancContas []*domain.LancamentoConta

	mapper := func(rows *sql.Rows) error {
		lancConta := &domain.LancamentoConta{}

		var lanc domain.Lancamento
		err := rows.Scan(
			&lanc.Id,
			&lanc.ContaId,
			&lanc.TipoLancamentoId,
			&lanc.Valor,
			&lanc.Descricao,
			&lanc.Emissao,
			&lanc.RegistroId,
			lancConta.ContaId,
			lancConta.ContaNome,
		)

		lancConta.SetLancamento(&lanc)
		lancContas = append(lancContas, lancConta)
		if err != nil {
			return err
		}

		return nil
	}

	err := r.adapter.FindAllQuery(mapper, lancFindByContaIdQuery, contaId, pg.GetPage(), pg.GetSize())
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func rollbackOnError(tx *sql.Tx, err error) error {
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return nil
}
