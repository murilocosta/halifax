package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
)

const (
	lancFutInsertQuery   = "INSERT INTO lancamentos_futuros (conta_destino_id, tipo_lancamento_destino_id, valor, descricao, validade, emissao, registroId) VALUES (?, ?, ?, ?, ?, ?, ?);"
	lancFutUpdateQuery   = "UPDATE lancamentos_futuros SET conta_destino_id = ?, tipo_lancamento_destino_id = ?, descricao = ?, validade = ? WHERE id = ?;"
	lancFutFindByIdQuery = "SELECT id, conta_destino_id, tipo_lancamento_destino_id, valor, descricao, validade, emissao, registroId FROM lancamentos_futuros WHERE id = ?;"
)

type lancamentoFuturoRepositoryMysql struct {
	db      *sql.DB
	adapter *core.MysqlAdapter
}

func NewLancamentoFuturoRepositoryMysql(db *sql.DB) domain.LancamentoFuturoRepository {
	return &lancamentoFuturoRepositoryMysql{
		db:      db,
		adapter: core.NewMysqlAdapter(db),
	}
}

func (r *lancamentoFuturoRepositoryMysql) Save(f *domain.LancamentoFuturo) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(
			f.ContaDestinoId,
			f.TipoLancamentoDestinoId,
			f.Valor,
			f.Descricao,
			f.Validade,
			f.Emissao,
			f.RegistroId,
		)
	}
	uid, err := r.adapter.SaveQuery(lancFutInsertQuery, executor)
	if err != nil {
		return err
	}
	f.Id = uid
	return nil
}

func (r *lancamentoFuturoRepositoryMysql) Update(t *domain.LancamentoFuturo) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(t.ContaDestinoId, t.TipoLancamentoDestinoId, t.Descricao, t.Validade, t.Id)
	}
	return r.adapter.UpdateQuery(lancFutUpdateQuery, executor)
}

func (r *lancamentoFuturoRepositoryMysql) FindById(id int64) (*domain.LancamentoFuturo, error) {
	var lancFut domain.LancamentoFuturo
	mapper := func(row *sql.Row) error {
		return row.Scan(
			&lancFut.Id,
			&lancFut.ContaDestinoId,
			&lancFut.TipoLancamentoDestinoId,
			&lancFut.Valor,
			&lancFut.Descricao,
			&lancFut.Validade,
			&lancFut.Emissao,
			&lancFut.RegistroId,
		)
	}
	err := r.adapter.FindByIdQuery(mapper, lancFutFindByIdQuery, id)
	if err != nil {
		return nil, err
	}
	return &lancFut, nil
}
