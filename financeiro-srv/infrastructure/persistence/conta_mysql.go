package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
)

const (
	insertContaQuery   = "INSERT INTO contas (nome) VALUES (?);"
	updateContaQuery   = "UPDATE contas SET nome = ? WHERE id = ?;"
	findByIdContaQuery = "SELECT id, nome from contas WHERE id = ?;"
	findAllContasQuery = "SELECT id, nome from contas LIMIT ? OFFSET ?;"
)

type contaRepositoryMysql struct {
	db      *sql.DB
	adapter *core.MysqlAdapter
}

func NewContaRepositoryMysql(db *sql.DB) domain.ContaRepository {
	return &contaRepositoryMysql{
		db:      db,
		adapter: core.NewMysqlAdapter(db),
	}
}

func (repo *contaRepositoryMysql) Save(c *domain.Conta) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(c.Nome)
	}

	uid, err := repo.adapter.SaveQuery(insertContaQuery, executor)
	if err != nil {
		return err
	}

	c.Id = uid
	return nil
}

func (repo *contaRepositoryMysql) Update(c *domain.Conta) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(c.Nome, c.Id)
	}

	return repo.adapter.UpdateQuery(updateContaQuery, executor)
}

func (repo *contaRepositoryMysql) FindById(id int64) (*domain.Conta, error) {
	var cnt domain.Conta
	mapper := func(row *sql.Row) error {
		return row.Scan(&cnt.Id, &cnt.Nome)
	}

	err := repo.adapter.FindByIdQuery(mapper, findByIdContaQuery, id)

	if err != nil {
		return nil, err
	}

	return &cnt, nil
}

func (repo *contaRepositoryMysql) FindAll(pg *core.Pagination) ([]*domain.Conta, error) {
	var cnts []*domain.Conta
	mapper := func(rows *sql.Rows) error {
		var cnt domain.Conta
		err := rows.Scan(&cnt.Id, &cnt.Nome)
		if err != nil {
			return err
		}
		cnts = append(cnts, &cnt)
		return nil
	}

	err := repo.adapter.FindAllQuery(mapper, findAllContasQuery, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	return cnts, nil
}
