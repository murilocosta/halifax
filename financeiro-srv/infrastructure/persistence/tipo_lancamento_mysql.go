package persistence

import (
	"database/sql"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
)

const (
	tlInsertQuery   = "INSERT INTO tipos_lancamento (nome) VALUES (?);"
	tlUpdateQuery   = "UPDATE tipos_lancamento SET nome = ? WHERE id = ?;"
	tlFindByIdQuery = "SELECT id, nome FROM tipos_lancamento WHERE id = ?;"
	tlFindAllQuery  = "SELECT id, nome FROM tipos_lancamento LIMIT ? OFFSET ?;"
)

type tipoLancamentoRepositoryMysql struct {
	db      *sql.DB
	adapter *core.MysqlAdapter
}

func NewTipoLancamentoRepositoryMysql(db *sql.DB) domain.TipoLancamentoRepository {
	return &tipoLancamentoRepositoryMysql{
		db:      db,
		adapter: core.NewMysqlAdapter(db),
	}
}

func (repo *tipoLancamentoRepositoryMysql) Save(t *domain.TipoLancamento) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(t.Nome)
	}
	uid, err := repo.adapter.SaveQuery(tlInsertQuery, executor)
	if err != nil {
		return err
	}
	t.Id = uid
	return nil
}

func (repo *tipoLancamentoRepositoryMysql) Update(t *domain.TipoLancamento) error {
	executor := func(stmt *sql.Stmt) (sql.Result, error) {
		return stmt.Exec(t.Nome, t.Id)
	}
	return repo.adapter.UpdateQuery(tlUpdateQuery, executor)
}

func (repo *tipoLancamentoRepositoryMysql) FindById(id int64) (*domain.TipoLancamento, error) {
	var tpLnc domain.TipoLancamento
	mapper := func(row *sql.Row) error {
		return row.Scan(&tpLnc.Id, &tpLnc.Nome)
	}

	err := repo.adapter.FindByIdQuery(mapper, tlFindByIdQuery, id)
	if err != nil {
		return nil, err
	}

	return &tpLnc, nil
}

func (repo *tipoLancamentoRepositoryMysql) FindAll(pg *core.Pagination) ([]*domain.TipoLancamento, error) {
	var tpLncs []*domain.TipoLancamento
	mapper := func(row *sql.Rows) error {
		var tpLnc domain.TipoLancamento
		err := row.Scan(&tpLnc.Id, &tpLnc.Nome)
		if err != nil {
			return err
		}
		tpLncs = append(tpLncs, &tpLnc)
		return nil
	}

	err := repo.adapter.FindAllQuery(mapper, tlFindAllQuery, pg.GetSize(), pg.GetPage())
	if err != nil {
		return nil, err
	}

	return tpLncs, nil
}
