package application

import (
	"time"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
)

type LancamentoUseCase struct {
	lancRepo domain.LancamentoRepository
	futRepo  domain.LancamentoFuturoRepository
}

func NewLancamentoUseCase(lancRepo domain.LancamentoRepository, futRepo domain.LancamentoFuturoRepository) *LancamentoUseCase {
	return &LancamentoUseCase{
		lancRepo: lancRepo,
		futRepo:  futRepo,
	}
}

func (ucase *LancamentoUseCase) Registrar(lanc *domain.Lancamento) error {
	err := ucase.lancRepo.Save(lanc)
	if err != nil {
		return err
	}
	return nil
}

func (ucase *LancamentoUseCase) Atualizar(id int64, tipoLancamentoId int64, descricao string, emissao time.Time) error {
	lanc, err := ucase.lancRepo.FindById(id)
	if err != nil {
		return err
	}
	lanc.Update(tipoLancamentoId, descricao, emissao)
	return ucase.lancRepo.Update(lanc)
}

func (ucase *LancamentoUseCase) Estornar(lancamentoId int64) (*domain.Lancamento, error) {
	lanc, err := ucase.lancRepo.FindById(lancamentoId)
	if err != nil {
		return nil, err
	}
	return ucase.lancRepo.Delete(lanc)
}

func (ucase *LancamentoUseCase) Agendar(lancFut *domain.LancamentoFuturo) error {
	err := ucase.futRepo.Save(lancFut)
	if err != nil {
		return err
	}
	return nil
}

func (ucase *LancamentoUseCase) ListarConta(contaId int64, page int32, size int32) ([]*domain.LancamentoConta, error) {
	pg := core.NewPagination(page, size)
	return ucase.lancRepo.FindByContaId(contaId, pg)
}
