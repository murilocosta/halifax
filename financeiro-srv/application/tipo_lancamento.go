package application

import (
	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
)

type TipoLancamentoUseCase struct {
	repo domain.TipoLancamentoRepository
}

func NewTipoLancamentoUseCase(repo domain.TipoLancamentoRepository) *TipoLancamentoUseCase {
	return &TipoLancamentoUseCase{
		repo:repo,
	}
}

func (ucase *TipoLancamentoUseCase) Criar(nome string) (int64, error) {
	t := domain.NewTipoLancamento(nome)
	err := ucase.repo.Save(t)
	if err != nil {
		return -1, err
	}
	return t.Id, nil
}

func (ucase *TipoLancamentoUseCase) Alterar(id int64, nome string) error {
	t, err := ucase.repo.FindById(id)
	if err != nil {
		return err
	}
	t.Update(nome)
	return ucase.repo.Update(t)
}

func (ucase *TipoLancamentoUseCase) Listar(page int32, size int32) ([]*domain.TipoLancamento, error) {
	pg := core.NewPagination(page, size)
	return ucase.repo.FindAll(pg)
}
