package application

import (
	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/domain"
)

type ContaUseCase struct {
	repo domain.ContaRepository
}

func NewContaUseCase(repo domain.ContaRepository) *ContaUseCase {
	return &ContaUseCase{
		repo: repo,
	}
}

func (u *ContaUseCase) Criar(nome string) (int64, error) {
	c := domain.NewConta(nome)
	err := u.repo.Save(c)
	if err != nil {
		return -1, err
	}
	return c.Id, nil
}

func (u *ContaUseCase) Alterar(id int64, nome string) error {
	c, err := u.repo.FindById(id)
	if err != nil {
		return err
	}
	c.Update(nome)
	err = u.repo.Update(c)
	if err != nil {
		return err
	}
	return nil
}

func (u *ContaUseCase) Listar(page int32, size int32) ([]*domain.Conta, error) {
	pg := core.NewPagination(page, size)
	return u.repo.FindAll(pg)
}
