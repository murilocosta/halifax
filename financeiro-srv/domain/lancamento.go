package domain

import (
	"time"

	"bitbucket.org/murilocosta/halifax/core"
)

type Lancamento struct {
	Id               int64
	ContaId          int64
	TipoLancamentoId int64
	Valor            float64
	Descricao        string
	Emissao          time.Time
	RegistroId       int64
}

func NewLancamento(registroId int64, contaId int64, tipoLancamentoId int64, valor float64, descricao string, emissao time.Time) *Lancamento {
	return &Lancamento{
		ContaId:          contaId,
		TipoLancamentoId: tipoLancamentoId,
		Valor:            valor,
		Descricao:        descricao,
		Emissao:          emissao,
		RegistroId:       registroId,
	}
}

func (l *Lancamento) Update(tipoLancamentoId int64, descricao string, emissao time.Time) {
	l.TipoLancamentoId = tipoLancamentoId
	l.Descricao = descricao
	l.Emissao = emissao
}

func (l *Lancamento) Estornar() *Lancamento {
	return &Lancamento{
		Id:               l.RegistroId,
		ContaId:          l.ContaId,
		TipoLancamentoId: l.TipoLancamentoId,
		Valor:            l.Valor * -1,
		Descricao:        l.Descricao,
		Emissao:          l.Emissao,
	}
}

type LancamentoConta struct {
	ContaId    int64
	ContaNome  string
	Lancamento *Lancamento
}

func (lc *LancamentoConta) SetLancamento(lanc *Lancamento) {
	lc.Lancamento = lanc
}

type LancamentoEstorno struct {
	Id        int64
	OrigemId  int64
	DestinoId int64
	Emissao   time.Time
}

func NewLancamentoEstorno(origemId int64, destinoId int64) *LancamentoEstorno {
	return &LancamentoEstorno{
		OrigemId:  origemId,
		DestinoId: destinoId,
		Emissao:   time.Now(),
	}
}

type LancamentoRepository interface {
	Save(l *Lancamento) error
	Update(l *Lancamento) error
	Delete(l *Lancamento) (*Lancamento, error)
	FindById(id int64) (*Lancamento, error)
	FindByContaId(contaId int64, pg *core.Pagination) ([]*LancamentoConta, error)
}
