package domain

import "bitbucket.org/murilocosta/halifax/core"

type Conta struct {
	Id   int64
	Nome string
}

func NewConta(nome string) *Conta {
	return &Conta{
		Nome: nome,
	}
}

func (c *Conta) Update(nome string) {
	c.Nome = nome
}

type ContaRepository interface {
	Save(c *Conta) error
	Update(c *Conta) error
	FindById(id int64) (*Conta, error)
	FindAll(pg *core.Pagination) ([]*Conta, error)
}
