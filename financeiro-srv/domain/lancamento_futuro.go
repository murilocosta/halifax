package domain

import (
	"time"
)

type LancamentoFuturo struct {
	Id                      int64
	ContaDestinoId          int64
	TipoLancamentoDestinoId int64
	Valor                   float64
	Descricao               string
	Validade                time.Time
	Emissao                 time.Time
	RegistroId              int64
}

func NewLancamentoFuturo(registroId int64, contaId int64, tipoLancamentoDestinoId int64, valor float64, descricao string, validate time.Time, emissao time.Time) *LancamentoFuturo {
	return &LancamentoFuturo{
		ContaDestinoId:          contaId,
		TipoLancamentoDestinoId: tipoLancamentoDestinoId,
		Valor:                   valor,
		Descricao:               descricao,
		Validade:                validate,
		Emissao:                 emissao,
		RegistroId:              registroId,
	}
}

func (l *LancamentoFuturo) Update(contaId int64, tipoLancamentoId int64, descricao string, validade time.Time) {
	l.ContaDestinoId = contaId
	l.TipoLancamentoDestinoId = tipoLancamentoId
	l.Descricao = descricao
	l.Validade = validade
}

type LancamentoFuturoRepository interface {
	Save(f *LancamentoFuturo) error
	Update(f *LancamentoFuturo) error
	FindById(id int64) (*LancamentoFuturo, error)
}
