package domain

import "bitbucket.org/murilocosta/halifax/core"

type TipoLancamento struct {
	Id   int64
	Nome string
}

func NewTipoLancamento(nome string) *TipoLancamento {
	return &TipoLancamento{
		Nome: nome,
	}
}

func (t *TipoLancamento) Update(nome string) {
	t.Nome = nome
}

type TipoLancamentoRepository interface {
	Save(t *TipoLancamento) error
	Update(t *TipoLancamento) error
	FindById(id int64) (*TipoLancamento, error)
	FindAll(pg *core.Pagination) ([]*TipoLancamento, error)
}
