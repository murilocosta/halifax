package subscriber

import (
	"context"
	"github.com/micro/go-micro/util/log"
)

type Financeiro struct{}

func (e *Financeiro) Handle(ctx context.Context, msg string) error {
	log.Log("Handler Received message: ", msg)
	return nil
}
