package main

import (
	"database/sql"
	"os"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/application"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/infrastructure/persistence"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/infrastructure/transport"
	"bitbucket.org/murilocosta/halifax/financeiro-srv/subscriber"

	_ "github.com/go-sql-driver/mysql"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"github.com/micro/go-micro/util/log"

	conta "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/conta"
	lancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/lancamento"
	tipoLancamento "bitbucket.org/murilocosta/halifax/financeiro-srv/proto/tipo-lancamento"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	dsl, _ := core.NewDataSourceLocation(
		config.DatabaseHost,
		config.DatabaseUser,
		config.DatabasePass,
		"halifax_financeiro",
	)
	db, err := sql.Open(config.DatabaseDriver, dsl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.financeiro"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	s := service.Server()
	registerContaHandler(db, s)
	registerTipoLancamentoHandler(db, s)
	registerLancamentoHandler(db, s)

	// Register Struct as Subscriber
	micro.RegisterSubscriber("go.micro.srv.financeiro", service.Server(), new(subscriber.Financeiro))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func registerContaHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewContaRepositoryMysql(db)
	ucase := application.NewContaUseCase(repo)
	handler := transport.NewContaHandlerGrpc(ucase)
	return conta.RegisterContaHandler(s, handler)
}

func registerTipoLancamentoHandler(db *sql.DB, s server.Server) error {
	repo := persistence.NewTipoLancamentoRepositoryMysql(db)
	ucase := application.NewTipoLancamentoUseCase(repo)
	handler := transport.NewTipoLancamentoHandlerGrpc(ucase)
	return tipoLancamento.RegisterTipoLancamentoHandler(s, handler)
}

func registerLancamentoHandler(db *sql.DB, s server.Server) error {
	lancRepo := persistence.NewLancamentoRepositoryMysql(db)
	futRepo := persistence.NewLancamentoFuturoRepositoryMysql(db)
	ucase := application.NewLancamentoUseCase(lancRepo, futRepo)
	handler := transport.NewLancamentoHandlerGrpc(ucase)
	return lancamento.RegisterLancamentoHandler(s, handler)
}
