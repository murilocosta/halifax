package client

import (
	"context"

	compras "bitbucket.org/murilocosta/halifax/compras-srv/proto/compra"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type comprasKey struct{}

func ComprasFromContext(ctx context.Context) (compras.CompraService, bool) {
	c, ok := ctx.Value(comprasKey{}).(compras.CompraService)
	return c, ok
}

func ComprasWrapper(service micro.Service) server.HandlerWrapper {
	client := compras.NewCompraService("go.micro.srv.compras", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, comprasKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
