package main

import (
	"os"

	"bitbucket.org/murilocosta/halifax/compras-api/client"
	"bitbucket.org/murilocosta/halifax/compras-api/handler"
	compras "bitbucket.org/murilocosta/halifax/compras-api/proto/compras"
	"bitbucket.org/murilocosta/halifax/core"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.api.compras"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init(
		micro.WrapHandler(client.ComprasWrapper(service)),
	)

	// Register Handler
	compras.RegisterComprasHandler(service.Server(), handler.NewComprasHandlerProxy())

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
