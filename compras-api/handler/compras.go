package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/compras-api/client"
	comprasApi "bitbucket.org/murilocosta/halifax/compras-api/proto/compras"
	compras "bitbucket.org/murilocosta/halifax/compras-srv/proto/compra"
	"bitbucket.org/murilocosta/halifax/core"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type comprasHandlerProxy struct{}

func NewComprasHandlerProxy() comprasApi.ComprasHandler {
	return &comprasHandlerProxy{}
}

func (*comprasHandlerProxy) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Compras.Criar request")

	comprasClient, ok := client.ComprasFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.compras", "compras client not found")
	}

	var payload compras.CriarRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.compras", err.Error())
	}

	response, err := comprasClient.Criar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.compras", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*comprasHandlerProxy) Alterar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Compras.Alterar request")

	comprasClient, ok := client.ComprasFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.compras", "compras client not found")
	}

	var payload compras.AlterarRequest
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.compras", err.Error())
	}

	response, err := comprasClient.Alterar(ctx, &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.compras", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*comprasHandlerProxy) Buscar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Compras.Buscar request")

	comprasClient, ok := client.ComprasFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.compras", "compras client not found")
	}

	id := core.ExtractValueFromRequestAsInt64(req.Get["id"], 0)
	response, err := comprasClient.Buscar(ctx, &compras.BuscarRequest{Id: id})
	if err != nil {
		return errors.BadRequest("go.micro.api.compras", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*comprasHandlerProxy) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Compras.Buscar request")

	comprasClient, ok := client.ComprasFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.compras", "compras client not found")
	}

	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := comprasClient.Listar(ctx, &compras.ListarRequest{
		Page: page * size,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.compras", err.Error())
	}

	var items []*compras.CompraMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.compras", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
