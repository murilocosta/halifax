package core

import (
	"database/sql"
	"github.com/micro/go-micro/util/log"
)

type ExecutePreparedStmt func(*sql.Stmt) (sql.Result, error)

type ExecuteScanRow func(*sql.Row) error

type ExecuteScanRows func(*sql.Rows) error

type MysqlAdapter struct {
	db *sql.DB
}

func NewMysqlAdapter(db *sql.DB) *MysqlAdapter {
	return &MysqlAdapter{
		db: db,
	}
}

func (adap *MysqlAdapter) SaveQuery(insertQuery string, execFunc ExecutePreparedStmt) (int64, error) {
	tx, err := adap.db.Begin()
	if err != nil {
		return -1, err
	}

	stmt, err := tx.Prepare(insertQuery)
	if err != nil {
		return -1, err
	}

	rs, err := execFunc(stmt)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return -1, err
	}

	uid, _ := rs.LastInsertId()
	err = tx.Commit()
	if err != nil {
		return -1, err
	}

	return uid, nil
}

func (adap *MysqlAdapter) UpdateQuery(updateQuery string, execFunc ExecutePreparedStmt) error {
	tx, err := adap.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(updateQuery)
	if err != nil {
		return err
	}

	_, err = execFunc(stmt)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Debug(rollbackErr)
		}
		return err
	}
	return tx.Commit()
}

func (adap *MysqlAdapter) FindByIdQuery(scanFunc ExecuteScanRow, findQuery string, uid int64) error {
	row := adap.db.QueryRow(findQuery, uid)

	err := scanFunc(row)
	if err != nil {
		return err
	}
	return nil
}

func (adap *MysqlAdapter) FindAllQuery(scanFunc ExecuteScanRows, findQuery string, args ...interface{}) error {
	rows, err := adap.db.Query(findQuery, args...)
	if err != nil {
		return err
	}
	for rows.Next() {
		err := scanFunc(rows)
		if err != nil {
			return err
		}
	}
	return rows.Close()
}
