package core

import (
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/config"
	"github.com/micro/go-micro/config/source/consul"
)

type Config struct {
	DatabaseDriver       string
	DatabaseHost         string
	DatabasePass         string
	DatabaseUser         string
	MicroBroker          string
	MicroBrokerAddress   string
	MicroRegistry        string
	MicroRegistryAddress string
	MicroTransport       string
}

type configKeyPair map[string]map[string]Config

func LoadConfigServer(consulHost string, resource string) (*Config, error) {
	kvSource := consul.NewSource(
		consul.WithAddress(consulHost),
		consul.WithPrefix(resource),
	)
	conf := config.NewConfig()
	err := conf.Load(kvSource)
	if err != nil {
		return nil, err
	}
	var secret configKeyPair
	err = conf.Scan(&secret)
	if err != nil {
		return nil, err
	}
	key, value := extractKeyValue(resource)
	c, _ := secret[key][value]
	return &c, nil
}

func extractKeyValue(resource string) (string, string) {
	parts := strings.Split(resource, "/")
	return parts[0], parts[1]
}

func NewDataSourceLocation(host string, user string, pass string, db string) (string, error) {
	params := &struct {
		Host     string
		User     string
		Pass     string
		Database string
	}{
		host,
		user,
		pass,
		db,
	}
	var buf strings.Builder
	dsl := "{{.User}}:{{.Pass}}@tcp({{.Host}})/{{.Database}}?parseTime=true"
	t, err := template.New("dsl").Parse(dsl)
	if err != nil {
		return "", err
	}
	err = t.Execute(&buf, params)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

type Pagination struct {
	page int32
	size int32
}

func NewPagination(page int32, size int32) *Pagination {
	return &Pagination{
		page: page,
		size: size,
	}
}

func (p *Pagination) GetPage() int32 {
	return p.page
}

func (p *Pagination) GetSize() int32 {
	return p.size
}

func ConvertToTimestamp(tempo time.Time) *timestamp.Timestamp {
	if tempo.IsZero() {
		return nil
	}
	return newTimestamp(tempo.Unix(), tempo.UnixNano())
}

func newTimestamp(seconds int64, nanos int64) *timestamp.Timestamp {
	return &timestamp.Timestamp{
		Seconds: seconds,
		Nanos:   int32(nanos),
	}
}

func ConvertFromTimestamp(tempo *timestamp.Timestamp) time.Time {
	if tempo == nil {
		return time.Time{}
	}
	return time.Unix(tempo.Seconds, int64(tempo.Nanos))
}

func ExtractValueFromRequest(pair *api.Pair, defaultValue string) string {
	if pair == nil {
		return defaultValue
	}
	if len(pair.Values) == 0 {
		return defaultValue
	}
	return pair.Values[0]
}

func ExtractValueFromRequestAsInt64(pair *api.Pair, defaultValue int64) int64 {
	v := ExtractValueFromRequest(pair, string(defaultValue))
	ret, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return defaultValue
	}
	return ret
}

func ExtractValueFromRequestAsInt32(pair *api.Pair, defaultValue int32) int32 {
	ret := ExtractValueFromRequestAsInt64(pair, int64(defaultValue))
	return int32(ret)
}
