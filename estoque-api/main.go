package main

import (
	"os"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-api/client"
	"bitbucket.org/murilocosta/halifax/estoque-api/handler"
	estoque "bitbucket.org/murilocosta/halifax/estoque-api/proto/estoque"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"
)

func main() {
	configServerHost := os.Getenv("CONFIG_SERVER")
	configServerKv := os.Getenv("CONFIG_SERVER_KV")

	config, err := core.LoadConfigServer(configServerHost, configServerKv)
	if err != nil {
		log.Fatal(err)
	}

	os.Setenv("MICRO_REGISTRY", config.MicroRegistry)
	os.Setenv("MICRO_REGISTRY_ADDRESS", config.MicroRegistryAddress)
	os.Setenv("MICRO_BROKER", config.MicroBroker)
	os.Setenv("MICRO_BROKER_ADDRESS", config.MicroBrokerAddress)
	os.Setenv("MICRO_TRANSPORT", config.MicroTransport)

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.api.estoque"),
		micro.Version("1.0.0"),
	)

	// Initialise service
	service.Init(
		micro.WrapHandler(client.EstoqueWrapper(service)),
		micro.WrapHandler(client.ProdutoWrapper(service)),
		micro.WrapHandler(client.ProdutoEstoqueWrapper(service)),
	)

	// Register Handler
	s := service.Server()
	estoque.RegisterEstoqueHandler(s, handler.NewEstoqueHandlerGateway())
	estoque.RegisterProdutoHandler(s, handler.NewProdutoHandlerGateway())
	estoque.RegisterProdutoEstoqueHandler(s, handler.NewProdutoEstoqueHandlerGateway())

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
