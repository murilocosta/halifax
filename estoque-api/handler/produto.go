package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-api/client"
	produtoApi "bitbucket.org/murilocosta/halifax/estoque-api/proto/estoque"
	produto "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type produtoHandlerGateway struct{}

func NewProdutoHandlerGateway() produtoApi.ProdutoHandler {
	return &produtoHandlerGateway{}
}

func (*produtoHandlerGateway) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Produto.Criar request")

	produtoClient, ok := client.ProdutoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto client not found")
	}

	var payload produto.ProdutoMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	response, err := produtoClient.Criar(ctx, &produto.CriarProdutoRequest{
		Produto: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*produtoHandlerGateway) Alterar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Produto.Alterar request")

	produtoClient, ok := client.ProdutoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto client not found")
	}

	var payload produto.ProdutoMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	response, err := produtoClient.Alterar(ctx, &produto.AlterarProdutoRequest{
		Produto: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*produtoHandlerGateway) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Produto.Listar request")

	produtoClient, ok := client.ProdutoFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto estoque client not found")
	}

	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := produtoClient.Listar(ctx, &produto.ListarProdutosRequest{
		Page: page,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	var items []*produto.ProdutoMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
