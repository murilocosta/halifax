package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-api/client"
	produtoEstoqueApi "bitbucket.org/murilocosta/halifax/estoque-api/proto/estoque"
	produtoEstoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto-estoque"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type produtoEstoqueHandlerGateway struct{}

func NewProdutoEstoqueHandlerGateway() produtoEstoqueApi.ProdutoEstoqueHandler {
	return &produtoEstoqueHandlerGateway{}
}

func (*produtoEstoqueHandlerGateway) Adicionar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received ProdutoEstoque.Adicionar request")

	produtoEstoqueClient, ok := client.ProdutoEstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto estoque client not found")
	}

	var payload produtoEstoque.ProdutoEstoqueMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	response, err := produtoEstoqueClient.Adicionar(ctx, &produtoEstoque.AdicionarProdutoEstoqueRequest{
		ProdutoEstoque: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*produtoEstoqueHandlerGateway) Remover(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received ProdutoEstoque.Remover request")

	produtoEstoqueClient, ok := client.ProdutoEstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto estoque client not found")
	}

	var payload produtoEstoque.ProdutoEstoqueMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	response, err := produtoEstoqueClient.Remover(ctx, &produtoEstoque.RemoverProdutoEstoqueRequest{
		ProdutoEstoque: &payload,
	})
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*produtoEstoqueHandlerGateway) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received ProdutoEstoque.Listar request")

	produtoEstoqueClient, ok := client.ProdutoEstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto estoque client not found")
	}

	estoqueId := core.ExtractValueFromRequestAsInt64(req.Get["estoqueId"], 0)
	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	response, err := produtoEstoqueClient.Listar(ctx, &produtoEstoque.ListarProdutosEstoqueRequest{
		EstoqueId: estoqueId,
		Page:      page,
		Size:      size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*produtoEstoqueHandlerGateway) VerificarDisponibilidade(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received ProdutoEstoque.VerificarDisponibilidade request")

	produtoEstoqueClient, ok := client.ProdutoEstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "produto estoque client not found")
	}

	stream, err := produtoEstoqueClient.VerificarDisponibilidade(ctx)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	var prodEsts []*produtoEstoque.ProdutoEstoqueMessage
	err = json.Unmarshal([]byte(req.Body), &prodEsts)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	for _, prodEst := range prodEsts {
		err = stream.Send(prodEst)
		if err != nil {
			return errors.BadRequest("go.micro.api.estoque", err.Error())
		}
	}

	var items []*produtoEstoque.ProdutoDisponibilidadeMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
