package handler

import (
	"context"
	"encoding/json"

	"bitbucket.org/murilocosta/halifax/core"
	"bitbucket.org/murilocosta/halifax/estoque-api/client"
	estoqueApi "bitbucket.org/murilocosta/halifax/estoque-api/proto/estoque"
	estoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/estoque"

	api "github.com/micro/go-micro/api/proto"
	"github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/util/log"
)

type estoqueHandlerGateway struct{}

func NewEstoqueHandlerGateway() estoqueApi.EstoqueHandler {
	return &estoqueHandlerGateway{}
}

func (*estoqueHandlerGateway) Criar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Estoque.Criar request")

	estoqueClient, ok := client.EstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "estoque client not found")
	}

	var payload estoque.EstoqueMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	response, err := estoqueClient.Criar(ctx, &estoque.CriarEstoqueRequest{
		Estoque: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 201
	rsp.Body = string(b)
	return nil
}

func (*estoqueHandlerGateway) Alterar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Estoque.Alterar request")

	estoqueClient, ok := client.EstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "estoque client not found")
	}

	var payload estoque.EstoqueMessage
	err := json.Unmarshal([]byte(req.Body), &payload)
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	response, err := estoqueClient.Alterar(ctx, &estoque.AlterarEstoqueRequest{
		Estoque: &payload,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(response)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}

func (*estoqueHandlerGateway) Listar(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Estoque.Listar request")

	estoqueClient, ok := client.EstoqueFromContext(ctx)
	if !ok {
		return errors.InternalServerError("go.micro.api.estoque", "estoque client not found")
	}

	page := core.ExtractValueFromRequestAsInt32(req.Get["page"], 1) - 1
	size := core.ExtractValueFromRequestAsInt32(req.Get["size"], 15)
	stream, err := estoqueClient.Listar(ctx, &estoque.ListarEstoquesRequest{
		Page: page,
		Size: size,
	})
	if err != nil {
		return errors.BadRequest("go.micro.api.estoque", err.Error())
	}

	var items []*estoque.EstoqueMessage
	for {
		item, err := stream.Recv()
		if err != nil {
			break
		}
		items = append(items, item)
	}

	err = stream.Close()
	if err != nil {
		return errors.InternalServerError("go.micro.api.estoque", err.Error())
	}

	b, _ := json.Marshal(items)
	rsp.StatusCode = 200
	rsp.Body = string(b)
	return nil
}
