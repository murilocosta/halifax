package client

import (
	"context"

	estoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/estoque"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type estoqueKey struct{}

func EstoqueFromContext(ctx context.Context) (estoque.EstoqueService, bool) {
	e, ok := ctx.Value(estoqueKey{}).(estoque.EstoqueService)
	return e, ok
}

func EstoqueWrapper(service micro.Service) server.HandlerWrapper {
	client := estoque.NewEstoqueService("go.micro.srv.estoque", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, estoqueKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
