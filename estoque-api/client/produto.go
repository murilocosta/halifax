package client

import (
	"context"

	produto "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type produtoKey struct{}

func ProdutoFromContext(ctx context.Context) (produto.ProdutoService, bool) {
	p, ok := ctx.Value(produtoKey{}).(produto.ProdutoService)
	return p, ok
}

// Client returns a wrapper for the EstoqueClient
func ProdutoWrapper(service micro.Service) server.HandlerWrapper {
	client := produto.NewProdutoService("go.micro.srv.estoque", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, produtoKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
