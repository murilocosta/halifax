package client

import (
	"context"

	produtoEstoque "bitbucket.org/murilocosta/halifax/estoque-srv/proto/produto-estoque"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
)

type produtoEstoqueKey struct{}

func ProdutoEstoqueFromContext(ctx context.Context) (produtoEstoque.ProdutoEstoqueService, bool) {
	p, ok := ctx.Value(produtoEstoqueKey{}).(produtoEstoque.ProdutoEstoqueService)
	return p, ok
}

func ProdutoEstoqueWrapper(service micro.Service) server.HandlerWrapper {
	client := produtoEstoque.NewProdutoEstoqueService("go.micro.srv.estoque", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, produtoEstoqueKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
